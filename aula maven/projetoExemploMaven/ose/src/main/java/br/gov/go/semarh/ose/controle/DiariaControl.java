package br.gov.go.semarh.ose.controle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.ambientjsf.util.UtilFacesRelatorio;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.util.UtilData;
import br.com.ambientinformatica.util.UtilException;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.DiariaSolicitada;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.entidade.EnumStatusDiaria;
import br.gov.go.semarh.ose.entidade.EnumTipoDiaria;
import br.gov.go.semarh.ose.entidade.EnumTipoTransporte;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.entidade.PrestacaoContaDocumento;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.negocio.DepartamentoNeg;
import br.gov.go.semarh.ose.negocio.DiariaNeg;
import br.gov.go.semarh.ose.negocio.DiariaUtilizadaNeg;
import br.gov.go.semarh.ose.negocio.FuncionarioNeg;
import br.gov.go.semarh.ose.negocio.RotaMunicipiosNeg;
import br.gov.go.semarh.ose.negocio.ValorDiariaNeg;
import br.gov.go.semarh.ose.persistencia.MunicipioDao;
import br.gov.go.semarh.ose.util.OseException;

@Controller("DiariaControl")
@Scope("conversation")
public class DiariaControl extends Controle{

	@Autowired
	private DiariaNeg diariaNeg;

	@Autowired
	private FuncionarioNeg funcionarioNeg;

	private List<Diaria> diarias = new ArrayList<Diaria>();

	private Diaria diaria = new Diaria();

	private Diaria diariaImprimir = new Diaria();

	private Integer numeroDiaria = 0; 

	private EnumStatusDiaria statusDiaria;

	private Funcionario requisitante;
	
	private Departamento departamento;


	private Date dataInicial = UtilData.getDataInicioMes(new Date());

	private Date dataFinal = UtilData.getDataFimMes(new Date());

	private Funcionario autoridadeEmitente = new Funcionario();

	private Funcionario autoridadeTitularOrgao = new Funcionario();

	private Funcionario funcionarioImprimir = new Funcionario();

	private Date dataInicio;

	private Date dataFim;

	private DiariaSolicitada diariaSolicitada = new DiariaSolicitada();

	private DiariaUtilizada diariaUtilizada = new DiariaUtilizada();

	private Funcionario servidor;

	private String descricaoAtividadesRealizadas;

	private String descricaoObjetivoViagem;

	private String programa;

	private String acao;

	private String fonteDeRecurso;

	private EnumUf uf = EnumUf.GO;

	@Autowired
	private ValorDiariaNeg valorDiariaNeg;
	
	@Autowired
	private DiariaUtilizadaNeg diariaUtilizadaNeg;
	
	@Autowired
	private DepartamentoNeg departamentoNeg;

	@Autowired
	private UsuarioLogadoControl usuarioLogadoControl;

	@Autowired
	private MunicipioDao municipioDao;

	@Autowired
	private RotaMunicipiosNeg rotaMunicipiosNeg;
	

	private RotaMunicipios rotaMunicipios = new RotaMunicipios();

	private Municipio municipio = new Municipio();

	private List<SelectItem> municipios = new ArrayList<SelectItem>();

	private EnumTipoTransporte tipoTransporte = EnumTipoTransporte.VEICULO_OFICIAL;

	private ValorDiaria valorAdotado;
	
	private List<PrestacaoContaDocumento> documentos = new ArrayList<PrestacaoContaDocumento>();


	@PostConstruct
	public void init(){
		try {
			requisitante = usuarioLogadoControl.getFuncionario();
			departamento = requisitante.getLotacao();
			listar(null);
			atualizarMunicipios();			
			valorAdotado = valorDiariaNeg.consultarAtivo();
			diariaSolicitada.setValorAdotado(valorAdotado.getValorDiariaEstado());
		} catch (OseException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	public void atualizarValorAdotado(){
		if(diariaSolicitada.getTipoDiaria()==EnumTipoDiaria.INTERIOR_GO || diariaSolicitada.getTipoDiaria()==EnumTipoDiaria.REGIAO_METROPOLITANA){
			diariaSolicitada.setValorAdotado(valorAdotado.getValorDiariaEstado());
		}else{
			diariaSolicitada.setValorAdotado(valorAdotado.getValorDiariaForaEstado());
		}
	}

	public void atualizarMunicipios(){
		try {
			municipios.clear();
			List<Municipio> municipiosList = municipioDao.listarPorUf(uf);
			for(Municipio m : municipiosList){
				municipios.add(new SelectItem(m, m.getDescricao()));
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public List<SelectItem> getUfs(){
		return UtilFaces.getListEnum(EnumUf.values());
	}

	public List<SelectItem> getStatusDiarias(){
		return UtilFaces.getListEnum(EnumStatusDiaria.values());
	}

	public void addMunicipio(ActionEvent evt){
		if(municipio !=null){
			diaria.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}
	}

	public void addRotaMunicipios(ActionEvent evt){
		if(rotaMunicipios != null){
			for(Municipio m : rotaMunicipios.getMunicipios()){
				diaria.addMunicipio(m);
			}
		}else{
			UtilFaces.addMensagemFaces("Selecione uma Rota.");
		}
	}

	public void removeMunicipio(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		diaria.removeMunicipios(municipio);
		municipio = new Municipio();
	}


	public void addMunicipioPC(ActionEvent evt){
		if(municipio !=null){
			diariaUtilizada.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}
	}

	public void removeMunicipioPC(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		diariaUtilizada.removeMunicipios(municipio);
		municipio = new Municipio();
	}
	
	public void uploadDocumento(FileUploadEvent event) {
		if(event.getFile() != null){
			PrestacaoContaDocumento documento = new PrestacaoContaDocumento();
			documento.setNomeDocumento(event.getFile().getFileName());
			documento.setDocumento(event.getFile().getContents());
			documentos.add(documento);
			UtilFaces.addMensagemFaces("Upload realizado com sucesso.");
		}else{
			UtilFaces.addMensagemFaces("Selecione um arquivo para realizar o upload.");
		}

	}  
	public void removeDocumento(ActionEvent evt){
		PrestacaoContaDocumento documento = new PrestacaoContaDocumento();
		documento = (PrestacaoContaDocumento) UtilFaces.getValorParametro(evt, "documentoRemover");
		if(documentos.contains(documento)){
			documentos.remove(documento);
		}
	}
	
	public void consultarDocumentos(ActionEvent evt){
		Diaria diaria= (Diaria) UtilFaces.getValorParametro(evt, "diariaVerDocumentos");
		try {
			documentos = diariaUtilizadaNeg.consultar(diaria.getDiariaUtilizada()).getDocumentos();
			
		} catch (PersistenciaException e) {			
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void replicarDiariaSolicitada(ActionEvent evt){
		diaria = new Diaria();
		diariaUtilizada = new DiariaUtilizada();
		documentos = new ArrayList<PrestacaoContaDocumento>();
		diaria = (Diaria) UtilFaces.getValorParametro(evt, "diariaPrestarConta");	
		diariaUtilizada.setAcrescimo(diaria.getDiariaSolicitada().isAcrescimo());
		diariaUtilizada.setHospedagem(diaria.getDiariaSolicitada().isHospedagem());
		diariaUtilizada.setQuantDiariaIntegral(diaria.getDiariaSolicitada().getQuantDiariaIntegral());
		diariaUtilizada.setQuantDiariaMeia(diaria.getDiariaSolicitada().getQuantDiariaMeia());
		diariaUtilizada.setTipoDiaria(diaria.getDiariaSolicitada().getTipoDiaria());
		diariaUtilizada.setTresQuartoDiaria(diaria.getDiariaSolicitada().getTresQuartoDiaria());
		diariaUtilizada.setUmQuartoDiaria(diaria.getDiariaSolicitada().getUmQuartoDiaria());
		diariaUtilizada.setValorAdotado(diaria.getDiariaSolicitada().getValorAdotado());
		descricaoAtividadesRealizadas = diaria.getDescricaoObjetivoViagem();
		diariaUtilizada.setDataInicio(diaria.getDataInicio());
		diariaUtilizada.setDataFim(diaria.getDataFim());
		diariaUtilizada.getMunicipios().clear();
		for(Municipio municipio : diaria.getMunicipios()){
			diariaUtilizada.addMunicipio(municipio);
		}


	}

	public void cancelar(ActionEvent evt){
		diaria.cancelarDiaria();
		try {
			diariaNeg.alterar(diaria);
		} catch (PersistenciaException e) {		
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void novaDiaria(ActionEvent evt){
		diaria = new Diaria();
	}

	public void atenderDiaria(ActionEvent evt){
		diaria = new Diaria();
		Diaria diariaAtender = (Diaria) UtilFaces.getValorParametro(evt, "diariaAtender");		 
		diaria = diariaAtender;
		requisitante = diaria.getRequisitante();
		servidor = diaria.getServidor();
		tipoTransporte = diaria.getTipoTransporte();
		dataFim = diaria.getDataFim();
		dataInicio = diaria.getDataInicio();
		descricaoObjetivoViagem = diaria.getDescricaoObjetivoViagem();
	}



	public List<SelectItem> getTiposDiarias(){
		return UtilFaces.getListEnum(EnumTipoDiaria.values());
	}

	public List<SelectItem> getTiposTransporte(){
		return UtilFaces.getListEnum(EnumTipoTransporte.values());
	}

	public void calcularDiaria(ActionEvent evt){

		if((diariaSolicitada.getQuantDiariaIntegral().compareTo(BigDecimal.ZERO) == 0) && diariaSolicitada.getQuantDiariaMeia().compareTo(BigDecimal.ZERO) == 0 && diariaSolicitada.getUmQuartoDiaria().compareTo(BigDecimal.ZERO) == 0 && diariaSolicitada.getTresQuartoDiaria().compareTo(BigDecimal.ZERO) == 0 ){
			if(!diariaSolicitada.isHospedagem()){
				diariaSolicitada.setQuantDiariaIntegral(BigDecimal.valueOf((dataFim.getTime() - dataInicio.getTime())/86400000));
				diariaSolicitada.setQuantDiariaMeia(new BigDecimal(0));
				diariaSolicitada.setUmQuartoDiaria(new BigDecimal(0));
				diariaSolicitada.setTresQuartoDiaria(new BigDecimal(0));
			}else{
				diariaSolicitada.setQuantDiariaIntegral(new BigDecimal(0));
				diariaSolicitada.setQuantDiariaMeia(BigDecimal.valueOf((dataFim.getTime() - dataInicio.getTime())/86400000));
				diariaSolicitada.setUmQuartoDiaria(new BigDecimal(0));
				diariaSolicitada.setTresQuartoDiaria(new BigDecimal(0));
			}
		}


	}


	public void listar(ActionEvent evt){
		diaria = new Diaria();
		diarias.clear();
		try {
			if(numeroDiaria > 0){
				diarias.add(diariaNeg.consultar(numeroDiaria));
			}else{
				diarias = diariaNeg.listar(dataInicial, dataFinal, servidor, statusDiaria, departamento );
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}   

	public List<Funcionario> consultarServidor(String query){
		List<Funcionario> funcionarios = new ArrayList<Funcionario>();
		try {
			funcionarios = funcionarioNeg.listar(null, query, null, null, null, null);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}

		return funcionarios;
	}

	public List<RotaMunicipios> consultarRotaMunicipios(String nome){
		List<RotaMunicipios> rotaMunicipiosList = new ArrayList<RotaMunicipios>();
		try {
			rotaMunicipiosList = rotaMunicipiosNeg.listar(null, nome);

		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
		return rotaMunicipiosList;
	}
	
	public List<Departamento> consultarDepartamento(String nome){
		try {
			return departamentoNeg.listar(null, nome);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
			return new ArrayList<Departamento>();
		}
	}


	public void imprimir(){
		try {
			if(diariaImprimir != null){
				diariaImprimir = diariaNeg.consultar(diariaImprimir.getId());

				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("diaria", diariaImprimir);
				UtilFacesRelatorio.gerarRelatorioFaces("jasper/diariaTotal.jasper", diariaImprimir.getMunicipios(), parametros);
			}
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		} catch (UtilException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void imprimirPrestarConta(){
		try {
			if(diariaImprimir != null){
				diariaImprimir = diariaNeg.consultar(diariaImprimir.getId());
				Calendar cFim = Calendar.getInstance();
				cFim.setTime(diariaImprimir.getDataFim());
				cFim.add(Calendar.DAY_OF_MONTH, 1);
				int diaSemana = cFim.get(Calendar.DAY_OF_WEEK);
				if(diaSemana == Calendar.SATURDAY) {
					cFim.add(Calendar.DAY_OF_MONTH, 2);
				} else if (diaSemana == Calendar.SUNDAY) {
					cFim.add(Calendar.DAY_OF_MONTH, 1);
				}
				Date dataRegistro = cFim.getTime();
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("diaria", diariaImprimir);
				parametros.put("dataRegistro", dataRegistro);
				parametros.put("saldo", diariaImprimir.getDiariaSolicitada().getValorTotal().subtract(diariaImprimir.getDiariaUtilizada().getValorTotal()));

				UtilFacesRelatorio.gerarRelatorioFaces("jasper/prestacaoContaDiaria.jasper", diariaImprimir.getMunicipios(), parametros);
			}
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		} catch (UtilException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}


	public void atender(ActionEvent evt){
		try {
			if(descricaoObjetivoViagem == null && descricaoObjetivoViagem.length() < 5){
				UtilFaces.addMensagemFaces("É necessário informar o Objetivo da Viagem");
			}    

			diaria.atender(autoridadeEmitente, autoridadeTitularOrgao, dataInicio, dataFim, diariaSolicitada, descricaoObjetivoViagem, servidor, requisitante);
			if(diaria.getTipoTransporte()==null){
				diaria.setTipoTransporte(tipoTransporte);				
			}

			diariaNeg.alterar(diaria);
			diaria = new Diaria();
			diariaSolicitada = new DiariaSolicitada();
		} catch (OseException e) {
			UtilFaces.addMensagemFaces(e);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void encerrar(ActionEvent evt){
		try{
			diariaUtilizada.setDocumentos(documentos);
			diariaUtilizada.setFuncionarioResponsavel(requisitante);
			diaria.encerrar(descricaoAtividadesRealizadas, diariaUtilizada);
			diariaNeg.alterar(diaria);
			diaria = new Diaria();
			diariaUtilizada = new DiariaUtilizada();
			descricaoAtividadesRealizadas = null;
		} catch(Exception e){
			UtilFaces.addMensagemFaces(e);
		}
	}   


	/*
	 * Getters and Setters
	 */
	public List<Diaria> getDiarias() {
		return diarias;
	}

	public void setDiarias(List<Diaria> diarias) {
		this.diarias = diarias;
	}

	public Diaria getDiaria() {
		return diaria;
	}

	public void setDiaria(Diaria diaria) {
		this.diaria = diaria;
	}

	public Integer getNumeroDiaria() {
		return numeroDiaria;
	}

	public void setNumeroDiaria(Integer numeroDiaria) {
		this.numeroDiaria = numeroDiaria;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Diaria getDiariaImprimir() {
		return diariaImprimir;
	}

	public void setDiariaImprimir(Diaria diariaImprimir) {
		this.diariaImprimir = diariaImprimir;
	}

	public Funcionario getAutoridadeEmitente() {
		return autoridadeEmitente;
	}

	public void setAutoridadeEmitente(Funcionario autoridadeEmitente) {
		this.autoridadeEmitente = autoridadeEmitente;
	}

	public Funcionario getAutoridadeTitularOrgao() {
		return autoridadeTitularOrgao;
	}

	public void setAutoridadeTitularOrgao(Funcionario autoridadeTitularOrgao) {
		this.autoridadeTitularOrgao = autoridadeTitularOrgao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public DiariaSolicitada getDiariaSolicitada() {
		return diariaSolicitada;
	}

	public void setDiariaSolicitada(DiariaSolicitada diariaSolicitada) {
		this.diariaSolicitada = diariaSolicitada;
	}

	public String getDescricaoAtividadesRealizadas() {
		return descricaoAtividadesRealizadas;
	}

	public void setDescricaoAtividadesRealizadas(
			String descricaoAtividadesRealizadas) {
		this.descricaoAtividadesRealizadas = descricaoAtividadesRealizadas;
	}

	public String getDescricaoObjetivoViagem() {
		return descricaoObjetivoViagem;
	}

	public void setDescricaoObjetivoViagem(String descricaoObjetivoViagem) {
		this.descricaoObjetivoViagem = descricaoObjetivoViagem;
	}

	public Funcionario getFuncionarioImprimir() {
		return funcionarioImprimir;
	}

	public void setFuncionarioImprimir(Funcionario funcionarioImprimir) {
		this.funcionarioImprimir = funcionarioImprimir;
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getFonteDeRecurso() {
		return fonteDeRecurso;
	}

	public void setFonteDeRecurso(String fonteDeRecurso) {
		this.fonteDeRecurso = fonteDeRecurso;
	}

	public DiariaNeg getDiariaNeg() {
		return diariaNeg;
	}

	public void setDiariaNeg(DiariaNeg diariaNeg) {
		this.diariaNeg = diariaNeg;
	}

	public FuncionarioNeg getFuncionarioNeg() {
		return funcionarioNeg;
	}

	public void setFuncionarioNeg(FuncionarioNeg funcionarioNeg) {
		this.funcionarioNeg = funcionarioNeg;
	}

	public Funcionario getServidor() {
		return servidor;
	}

	public void setServidor(Funcionario servidor) {
		this.servidor = servidor;
	}

	public MunicipioDao getMunicipioDao() {
		return municipioDao;
	}

	public void setMunicipioDao(MunicipioDao municipioDao) {
		this.municipioDao = municipioDao;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<SelectItem> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<SelectItem> municipios) {
		this.municipios = municipios;
	}

	public EnumTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(EnumTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}

	public DiariaUtilizada getDiariaUtilizada() {
		return diariaUtilizada;
	}

	public void setDiariaUtilizada(DiariaUtilizada diariaUtilizada) {
		this.diariaUtilizada = diariaUtilizada;
	}

	public EnumStatusDiaria getStatusDiaria() {
		return statusDiaria;
	}

	public void setStatusDiaria(EnumStatusDiaria statusDiaria) {
		this.statusDiaria = statusDiaria;
	}

	public RotaMunicipios getRotaMunicipios() {
		return rotaMunicipios;
	}

	public void setRotaMunicipios(RotaMunicipios rotaMunicipios) {
		this.rotaMunicipios = rotaMunicipios;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public List<PrestacaoContaDocumento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<PrestacaoContaDocumento> documentos) {
		this.documentos = documentos;
	}   

}
