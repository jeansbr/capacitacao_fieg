package br.gov.go.semarh.ose.controle;

import javax.annotation.PostConstruct;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller("InicioControl")
@Scope("session")
public class InicioControl {

	private DashboardModel model;
	
	@PostConstruct
	public void init(){
        model = new DefaultDashboardModel();  
        DashboardColumn column1 = new DefaultDashboardColumn();  
        DashboardColumn column2 = new DefaultDashboardColumn();  
          
        column1.addWidget("atribuido");  
        column1.addWidget("noticia");  
          
        column2.addWidget("etc");  
  
        model.addColumn(column1);  
        model.addColumn(column2);  
	}

	public DashboardModel getModel() {
		return model;
	}
	
	
	
}
