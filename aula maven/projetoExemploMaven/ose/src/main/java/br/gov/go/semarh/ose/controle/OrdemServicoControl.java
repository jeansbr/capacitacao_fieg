package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.ambientjsf.util.UtilFacesRelatorio;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.gov.go.semarh.ose.entidade.Cargo;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemServico;
import br.gov.go.semarh.ose.entidade.EnumStatusVeiculo;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.entidade.OrdemServico;
import br.gov.go.semarh.ose.entidade.OrdemTrafego;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.entidade.ServicoExecutado;
import br.gov.go.semarh.ose.negocio.DepartamentoNeg;
import br.gov.go.semarh.ose.negocio.DiariaNeg;
import br.gov.go.semarh.ose.negocio.FuncionarioNeg;
import br.gov.go.semarh.ose.negocio.OrdemServicoNeg;
import br.gov.go.semarh.ose.negocio.OrdemTrafegoNeg;
import br.gov.go.semarh.ose.negocio.RotaMunicipiosNeg;
import br.gov.go.semarh.ose.negocio.ServicoNeg;
import br.gov.go.semarh.ose.negocio.VeiculoNeg;
import br.gov.go.semarh.ose.persistencia.MunicipioDao;

@Controller("OrdemServicoControl")
@Scope("conversation")
public class OrdemServicoControl extends Controle {

	private OrdemServico ordemServico;

	private OrdemServico osCancelar;

	private OrdemServico ordemServicoExcluir;

	private List<OrdemServico> ordemServicos = new ArrayList<OrdemServico>();

	private Integer id;

	private Funcionario requisitante;
	
	private Funcionario requisitanteList;

	private Funcionario servidor;

	private Servico servico = new Servico();

	private ServicoExecutado servicoExecutado = new ServicoExecutado();

	private Date dataHoraInicio;

	private Date dataHoraFim;

	private Date data;
	
	private Departamento departamento;

	private List<SelectItem> municipios = new ArrayList<SelectItem>();
	
	
	
	private RotaMunicipios rotaMunicipios = new RotaMunicipios();

	private Municipio municipio = new Municipio();

	private EnumStatusOrdemServico estadoOsSelecionado;

	@Autowired
	private MunicipioDao municipioDao;
	
	@Autowired
	private RotaMunicipiosNeg rotaMunicipiosNeg;

	@Autowired
	private OrdemServicoNeg ordemServicoNeg;

	@Autowired
	private FuncionarioNeg funcionarioNeg;
	
	@Autowired
	private DepartamentoNeg departamentoNeg;

	@Autowired
	private ServicoNeg servicoNeg;

	@Autowired
	private DiariaNeg diariaNeg;

	@Autowired
	private OrdemTrafegoNeg ordemTrafegoNeg;

	@Autowired
	private VeiculoNeg veiculoNeg;

	@Autowired
	private UsuarioLogadoControl usuarioLogadoControl;

	private EnumUf uf = EnumUf.GO;

	public List<SelectItem> getUfs(){
		return UtilFaces.getListEnum(EnumUf.values());
	}

	@PostConstruct
	public void init(){
		atualizarMunicipios();
		requisitante = usuarioLogadoControl.getFuncionario();
		departamento = requisitante.getLotacao();
		listar(null);
		ordemServico = new OrdemServico(requisitante);
	}

	public List<SelectItem> getTiposTransportes(){
		return UtilFaces.getListEnum(br.gov.go.semarh.ose.entidade.EnumTipoTransporte.values());
	}   

	public List<SelectItem> getStatusOrdemServicos(){
		return UtilFaces.getListEnum(br.gov.go.semarh.ose.entidade.EnumStatusOrdemServico.values());
	}

	public void atualizarMunicipios(){
		try {
			municipios.clear();
			List<Municipio> municipiosList = municipioDao.listarPorUf(uf);
			for(Municipio m : municipiosList){
				municipios.add(new SelectItem(m, m.getDescricao()));
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	

	public void salvar(ActionEvent evt){
		try {
			if(ordemServico.getDataHoraInicio().before(ordemServico.getDataHoraFim())){
				ordemServico = ordemServicoNeg.alterar(ordemServico);
				UtilFaces.addMensagemFaces("Ordem de Servico salva com sucesso");
			}else{
				UtilFaces.addMensagemFaces("Data de saida maior que a data de retorno");	
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void editar(ActionEvent evt){
		ordemServico = new OrdemServico(requisitante);
		ordemServico = (OrdemServico) UtilFaces.getValorParametro(evt, "os");
	}



	public void imprimir(ActionEvent evt){
		try {
			OrdemServico osImprimir = (OrdemServico) UtilFaces.getValorParametro(evt, "osImprimir");
			Cargo cargoRequisitante = funcionarioNeg.consultar(osImprimir.getRequisitante().getId()).getCargo();
			if(osImprimir != null){
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("ordemServico", osImprimir);
				parametros.put("cargoRequisitante", cargoRequisitante);
				UtilFacesRelatorio.gerarRelatorioFaces("jasper/ordemServico.jasper", osImprimir.getFuncionariosList(), parametros);
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	public void imprimirOT(ActionEvent evt){
		try {
			OrdemServico osImprimir = (OrdemServico) UtilFaces.getValorParametro(evt, "otImprimir");
			if(osImprimir != null){
				OrdemTrafego ordemTrafegoImprimir = ordemTrafegoNeg.consultar(osImprimir.getOrdemTrafego().getId());
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("ordemTrafego", ordemTrafegoImprimir);
				UtilFacesRelatorio.gerarRelatorioFaces("jasper/ordemTrafego.jasper", ordemTrafegoImprimir.getMunicipios(), parametros);
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void cancelar(ActionEvent evt){
		try {
			osCancelar = ordemServicoNeg.consultar(osCancelar.getId());
			osCancelar.cancelarOrdemServico();
			if(osCancelar.getOrdemTrafego() != null){
				osCancelar.getOrdemTrafego().cancelarOrdemTrafego();
				if(osCancelar.getOrdemTrafego().getVeiculo() != null){
					osCancelar.getOrdemTrafego().getVeiculo().setStatusVeiculo(EnumStatusVeiculo.LIVRE);
					veiculoNeg.alterar(osCancelar.getOrdemTrafego().getVeiculo());
				}
				ordemTrafegoNeg.alterar(osCancelar.getOrdemTrafego());
			}
			if(osCancelar.getDiarias() != null && !osCancelar.getDiarias().isEmpty()){
				for(Diaria diaria : osCancelar.getDiarias()){
					diaria.cancelarDiaria();
					diariaNeg.alterar(diaria);
				}
			}
			ordemServicoNeg.alterar(osCancelar);
			listar(null);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}


	public void confirmar(){
		try{
			if(ordemServico.getDataHoraInicio().before(ordemServico.getDataHoraFim())){
				if(ordemServico.getServicosExecutados() != null && !ordemServico.getServicosExecutados().isEmpty()){
				ordemServicoNeg.confirmarOrdemServico(ordemServico);
				limpar(null);
				UtilFaces.addMensagemFaces("Ordem de Serviço Emitida");	
				}else{
					UtilFaces.addMensagemFaces("Informe o serviço a ser executado");	
				}
				
			}else{
				UtilFaces.addMensagemFaces("Data de saida maior que a data de retorno");	
			}

		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);

		}

	}

	public void addMunicipio(ActionEvent evt){
		if(municipio !=null){
			ordemServico.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}

	}
	
	public void addRotaMunicipios(ActionEvent evt){
		if(rotaMunicipios != null){
			for(Municipio m : rotaMunicipios.getMunicipios()){
				ordemServico.addMunicipio(m);
			}
		}else{
			UtilFaces.addMensagemFaces("Selecione uma Rota.");
		}
	}

	public void removeMunicipio(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		ordemServico.removeMunicipios(municipio);
		municipio = new Municipio();
	}
	public void addServidor(ActionEvent evt){
		if(servidor != null){
			ordemServico.addFuncionario(servidor);
			servidor = new Funcionario();
		}else {
			UtilFaces.addMensagemFaces("Selecione um servidor");
		}
	}


	public void removeServidor(ActionEvent evt){
		servidor = new Funcionario();
		servidor = (Funcionario) UtilFaces.getValorParametro(evt, "servidorRemover");
		ordemServico.removeFuncionario(servidor);
		servidor = new Funcionario();
	}


	public void addServico(ActionEvent evt){
		if(servico != null){
			servicoExecutado.setServico(servico);
			ordemServico.addServicosExecutados(servicoExecutado);
			servico = new Servico();
			servicoExecutado = new ServicoExecutado();
		} else {
			UtilFaces.addMensagemFaces("Selecione um serviço");
		}
	}

	public List<Funcionario> consultarServidor(String nome){
		try {
			return funcionarioNeg.listar(null, nome, null, null, null, null);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
			return new ArrayList<Funcionario>();
		}
	}
	
	public List<Departamento> consultarDepartamento(String nome){
		try {
			return departamentoNeg.listar(null, nome);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
			return new ArrayList<Departamento>();
		}
	}
	

	public List<Servico> consultarServico(String query){
		List<Servico> servicos = new ArrayList<Servico>();
		try {
			servicos = servicoNeg.listar(null, query);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
		return servicos;
	}
	
	public List<RotaMunicipios> consultarRotaMunicipios(String nome){
		List<RotaMunicipios> rotaMunicipiosList = new ArrayList<RotaMunicipios>();
		try {
			rotaMunicipiosList = rotaMunicipiosNeg.listar(null, nome);
			
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
		return rotaMunicipiosList;
	}

	public void listar(ActionEvent evt){
		try {
			ordemServicos.clear();
			ordemServicos = ordemServicoNeg.listar(id, requisitanteList, dataHoraInicio, dataHoraFim, data, estadoOsSelecionado, departamento);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void limpar(ActionEvent evt){
		ordemServico = new OrdemServico(requisitante);
		servicoExecutado = new ServicoExecutado();
	}

	public void excluir(ActionEvent evt){
		try {
			if(ordemServicoExcluir != null){
				ordemServicoNeg.excluirPorId(ordemServicoExcluir.getId());
			}
			listar(evt);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	/*
	 * Getters and Setters
	 */
	public List<OrdemServico> getOrdemServicos() {
		return ordemServicos;
	}

	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		try {
			if(ordemServico != null){
				ordemServico = ordemServicoNeg.consultar(ordemServico.getId());
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
		this.ordemServico = ordemServico;
	}

	public OrdemServico getOrdemServicoExcluir() {
		return ordemServicoExcluir;
	}

	public void setOrdemServicoExcluir(OrdemServico ordemServicoExcluir) {
		this.ordemServicoExcluir = ordemServicoExcluir;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Date getDataHoraInicio() {
		return dataHoraInicio;
	}

	public void setDataHoraInicio(Date dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public Date getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Funcionario getServidor() {
		return servidor;
	}

	public void setServidor(Funcionario servidor) {
		this.servidor = servidor;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public ServicoExecutado getServicoExecutado() {
		return servicoExecutado;
	}

	public void setServicoExecutado(ServicoExecutado servicoExecutado) {
		this.servicoExecutado = servicoExecutado;
	}

	public List<SelectItem> getMunicipios() {
		return municipios;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public EnumStatusOrdemServico getEstadoOsSelecionado() {
		return estadoOsSelecionado;
	}

	public void setEstadoOsSelecionado(EnumStatusOrdemServico estadoOsSelecionado) {
		this.estadoOsSelecionado = estadoOsSelecionado;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}

	public OrdemServico getOsCancelar() {
		return osCancelar;
	}

	public void setOsCancelar(OrdemServico osCancelar) {
		this.osCancelar = osCancelar;
	}

	public RotaMunicipios getRotaMunicipios() {
		return rotaMunicipios;
	}

	public void setRotaMunicipios(RotaMunicipios rotaMunicipios) {
		this.rotaMunicipios = rotaMunicipios;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Funcionario getRequisitanteList() {
		return requisitanteList;
	}

	public void setRequisitanteList(Funcionario requisitanteList) {
		this.requisitanteList = requisitanteList;
	}

	
}
