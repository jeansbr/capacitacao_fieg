package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.ambientjsf.util.UtilFacesRelatorio;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemTrafego;
import br.gov.go.semarh.ose.entidade.EnumStatusVeiculo;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.entidade.OrdemServico;
import br.gov.go.semarh.ose.entidade.OrdemTrafego;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.negocio.DepartamentoNeg;
import br.gov.go.semarh.ose.negocio.FuncionarioNeg;
import br.gov.go.semarh.ose.negocio.OrdemTrafegoNeg;
import br.gov.go.semarh.ose.negocio.RotaMunicipiosNeg;
import br.gov.go.semarh.ose.negocio.ServicoNeg;
import br.gov.go.semarh.ose.negocio.VeiculoNeg;
import br.gov.go.semarh.ose.persistencia.MunicipioDao;

@Controller("OrdemTrafegoControl")
@Scope("conversation")
public class OrdemTrafegoControl extends Controle {

	private Integer id;

	private OrdemServico ordemServicoExcluir;

	private List<OrdemTrafego> ordensTrafego = new ArrayList<OrdemTrafego>();

	private OrdemTrafego ordemTrafego = new OrdemTrafego();

	private OrdemTrafego ordemTrafegoImprimir;

	private Funcionario motorista = new Funcionario();

	private Funcionario requisitante = new Funcionario();

	private Funcionario servidor = new Funcionario();
	
	private Departamento departamento;

	private Veiculo veiculo = new Veiculo();

	private Veiculo veiculoPesq;

	private Date dataHoraSaida = new Date();

	private Date dataHoraRetorno = new Date();

	private Date dataPrevistaSaida;

	private Date dataPrevistaRetorno;

	private Servico servico;

	private RotaMunicipios rotaMunicipios = new RotaMunicipios();

	private boolean viagem;

	@Autowired
	private FuncionarioNeg funcionarioNeg;

	@Autowired
	private ServicoNeg servicoNeg;

	@Autowired
	private OrdemTrafegoNeg ordemTrafegoNeg;

	@Autowired
	private VeiculoNeg veiculoNeg;

	@Autowired
	private RotaMunicipiosNeg rotaMunicipiosNeg;
	
	@Autowired
	private DepartamentoNeg departamentoNeg;

	private Date dataInicial;

	private Date dataFinal;

	private Integer hodometroRetorno;

	@Autowired
	private MunicipioDao municipioDao;

	private Municipio municipio = new Municipio();

	private List<SelectItem> municipios = new ArrayList<SelectItem>();

	private EnumUf uf = EnumUf.GO;

	private EnumStatusOrdemTrafego statusSelecionadoOT;

	@Autowired
	private UsuarioLogadoControl usuarioLogadoControl;


	@PostConstruct
	public void init(){
		listar(null);
		atualizarMunicipios();		

	}

	public List<SelectItem> getUfs(){
		return UtilFaces.getListEnum(EnumUf.values());
	}


	public List<SelectItem> getStatusOtList(){
		return UtilFaces.getListEnum(EnumStatusOrdemTrafego.values());
	}

	public void atualizarMunicipios(){
		try {
			municipios.clear();
			List<Municipio> municipiosList = municipioDao.listarPorUf(uf);
			for(Municipio m : municipiosList){
				municipios.add(new SelectItem(m, m.getDescricao()));
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void addMunicipio(ActionEvent evt){
		if(municipio !=null){
			ordemTrafego.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}
	}

	public void addRotaMunicipios(ActionEvent evt){
		if(rotaMunicipios != null){
			for(Municipio m : rotaMunicipios.getMunicipios()){
				ordemTrafego.addMunicipio(m);
			}
		}else{
			UtilFaces.addMensagemFaces("Selecione uma Rota.");
		}
	}

	public void removeMunicipio(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		ordemTrafego.removeMunicipios(municipio);
		municipio = new Municipio();
	}

	public void addServidor(ActionEvent evt){
		if(servidor != null){
			ordemTrafego.addFuncionario(servidor);
			servidor = new Funcionario();
		}else {
			UtilFaces.addMensagemFaces("Selecione um servidor");
		}
	}

	public void removeServidor(ActionEvent evt){
		servidor = new Funcionario();
		servidor = (Funcionario) UtilFaces.getValorParametro(evt, "servidorRemover");
		ordemTrafego.removeFuncionario(servidor);
		servidor = new Funcionario();
	}

	public void addServico(ActionEvent evt){
		if(servico != null){
			ordemTrafego.addServico(servico);
		} else {
			UtilFaces.addMensagemFaces("Selecione um serviço");
		}
	}

	public void novaOT(ActionEvent evt){
		ordemTrafego = new OrdemTrafego();
		ordemTrafego.novaOT();
	}

	public String limparFormulario(){
		this.ordemTrafego = new OrdemTrafego();
		return "ordemTrafegoList.semarh";

	}

	public List<RotaMunicipios> consultarRotaMunicipios(String nome){
		List<RotaMunicipios> rotaMunicipiosList = new ArrayList<RotaMunicipios>();
		try {
			rotaMunicipiosList = rotaMunicipiosNeg.listar(null, nome);

		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
		return rotaMunicipiosList;
	}

	public List<Funcionario> consultarServidor(String query){
		List<Funcionario> funcionarios = new ArrayList<Funcionario>();
		try {
			funcionarios = funcionarioNeg.listar(null, query, null, null, null, null);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}

		return funcionarios;
	}

	public List<Veiculo> consultarVeiculo(String query){
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		try {
			veiculos = veiculoNeg.listar(null, query, null, null, null, null);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}
		return veiculos;
	}

	public List<Servico> consultarServico(String query){
		List<Servico> servicos = new ArrayList<Servico>();
		try {
			servicos = servicoNeg.listar(null, query);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}
		return servicos;
	}
	
	public List<Departamento> consultarDepartamento(String nome){
		try {
			return departamentoNeg.listar(null, nome);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
			return new ArrayList<Departamento>();
		}
	}

	public void autorizarOT(ActionEvent evt){

		try {
			ordemTrafego.autorizar(usuarioLogadoControl.getFuncionario(), ordemTrafegoNeg.numeroDaAutorizacao());
			ordemTrafegoNeg.alterar(ordensTrafego);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void atenderOT(ActionEvent evt){
		ordemTrafego = (OrdemTrafego) UtilFaces.getValorParametro(evt, "otAtender");
		dataHoraSaida = ordemTrafego.getDataHoraSaida();
		motorista = ordemTrafego.getMotorista();
	}

	public void desatenderOT(ActionEvent evt){
		try {

			veiculo = veiculoNeg.consultar(ordemTrafego.getVeiculo().getId());
			veiculo.setStatusVeiculo(EnumStatusVeiculo.LIVRE);
			veiculoNeg.alterar(veiculo);
			ordemTrafego.desatender();
			ordemTrafegoNeg.alterar(ordemTrafego);

		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}

	}

	public void confirmar(ActionEvent evt){
		try {
			if( ordemTrafego.getMunicipios()!=null && !ordemTrafego.getMunicipios().isEmpty() ){
				if(ordemTrafego.getServicos()!= null && !ordemTrafego.getServicos().isEmpty()){
					veiculo.setStatusVeiculo(EnumStatusVeiculo.OCUPADO);
					veiculoNeg.alterar(veiculo);
					ordemTrafego.confirmarOT(veiculo, dataHoraSaida, motorista, requisitante, viagem, dataPrevistaRetorno, usuarioLogadoControl.getFuncionario());
					ordemTrafegoNeg.alterar(ordemTrafego);

					this.limparFormulario();
					UtilFaces.addMensagemFaces("Ordem de Tráfego confirmada."); 
				}else{
					UtilFaces.addMensagemFaces("Informe o serviço a ser executado");	
				}
					
			}
			else {
				UtilFaces.addMensagemFaces("Não foi gerada a Ordem de Tráfego, pois não foi adicionado um Itinerário. ");
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void encerrarOT(ActionEvent evt){
		ordemTrafego = (OrdemTrafego) UtilFaces.getValorParametro(evt, "otEncerrar");
	}

	public void desencerrarOT(ActionEvent evt){		
		try {
			veiculo = veiculoNeg.consultar(ordemTrafego.getVeiculo().getId());
			veiculo.setStatusVeiculo(EnumStatusVeiculo.OCUPADO);
			veiculo.setHodometro(ordemTrafego.getHodometroSaida());
			veiculoNeg.alterar(veiculo);
			ordemTrafego.desencerrar();
			ordemTrafegoNeg.alterar(ordemTrafego);
		} catch (PersistenciaException e) {			
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void encerrar(ActionEvent evt){
		try{
			if(ordemTrafego.getVeiculo().getHodometro()<hodometroRetorno){
				ordemTrafego.encerrar(hodometroRetorno, dataHoraRetorno);
				veiculo = veiculoNeg.consultar(ordemTrafego.getVeiculo().getId());
				veiculo.setStatusVeiculo(EnumStatusVeiculo.LIVRE);
				veiculo.setHodometro(hodometroRetorno);
				veiculoNeg.alterar(veiculo);
				ordemTrafegoNeg.alterar(ordemTrafego);
				if(ordemTrafego != null){
					ordemTrafego = ordemTrafegoNeg.consultar(ordemTrafego.getId());
				}

				this.limparFormulario(); 
				UtilFaces.addMensagemFaces("Ordem de Tráfego encerrada.");
			}
			else{
				UtilFaces.addMensagemFaces("Hodometro de retorno é menor que o de saida.");  
			}
		} catch(Exception e){
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void cancelar(ActionEvent evt){
		try {
			if(ordemTrafego.getVeiculo()!=null){
				veiculo = veiculoNeg.consultar(ordemTrafego.getVeiculo().getId());
				veiculo.setStatusVeiculo(EnumStatusVeiculo.LIVRE);
				veiculoNeg.alterar(veiculo);
			}
			ordemTrafego.cancelarOrdemTrafego();
			ordemTrafegoNeg.alterar(ordensTrafego);
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}



	public void imprimir(){
		try {
			if(ordemTrafegoImprimir != null){
				ordemTrafegoImprimir = ordemTrafegoNeg.consultar(ordemTrafegoImprimir.getId());
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("ordemTrafego", ordemTrafegoImprimir);
				UtilFacesRelatorio.gerarRelatorioFaces("jasper/ordemTrafego.jasper", ordemTrafegoImprimir.getMunicipios(), parametros);
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void listar(ActionEvent evt){
		try {     

			ordensTrafego = ordemTrafegoNeg.listar(id, requisitante, dataInicial, dataFinal, statusSelecionadoOT, veiculoPesq, departamento);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}


	/*
	 * Getters ande Setters
	 */
	public Funcionario getRequisitante() {
		return requisitante;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Funcionario getMotorista() {
		return motorista;
	}

	public void setMotorista(Funcionario motorista) {
		this.motorista = motorista;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Date getDataHoraSaida() {
		return dataHoraSaida;
	}

	public void setDataHoraSaida(Date dataHoraSaida) {
		this.dataHoraSaida = dataHoraSaida;
	}

	public Date getDataHoraRetorno() {
		return dataHoraRetorno;
	}

	public void setDataHoraRetorno(Date dataHoraRetorno) {
		this.dataHoraRetorno = dataHoraRetorno;
	}

	public OrdemTrafego getOrdemTrafego() {
		return ordemTrafego;
	}

	public void setOrdemTrafego(OrdemTrafego ordemTrafego) {
		this.ordemTrafego = ordemTrafego;
	}

	public OrdemServico getOrdemServicoExcluir() {
		return ordemServicoExcluir;
	}

	public void setOrdemServicoExcluir(OrdemServico ordemServicoExcluir) {
		this.ordemServicoExcluir = ordemServicoExcluir;
	}

	public List<OrdemTrafego> getOrdensTrafego() {
		return ordensTrafego;
	}

	public void setOrdensTrafego(List<OrdemTrafego> ordensTrafego) {
		this.ordensTrafego = ordensTrafego;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Integer getHodometroRetorno() {
		return hodometroRetorno;
	}

	public void setHodometroRetorno(Integer hodometroRetorno) {
		this.hodometroRetorno = hodometroRetorno;
	}

	public OrdemTrafego getOrdemTrafegoImprimir() {
		return ordemTrafegoImprimir;
	}

	public void setOrdemTrafegoImprimir(OrdemTrafego ordemTrafegoImprimir) {
		this.ordemTrafegoImprimir = ordemTrafegoImprimir;
	}

	public Funcionario getServidor() {
		return servidor;
	}

	public void setServidor(Funcionario servidor) {
		this.servidor = servidor;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<SelectItem> getMunicipios() {
		return municipios;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public boolean isViagem() {
		return viagem;
	}

	public void setViagem(boolean viagem) {
		this.viagem = viagem;
	}

	public Date getDataPrevistaSaida() {
		return dataPrevistaSaida;
	}

	public void setDataPrevistaSaida(Date dataPrevistaSaida) {
		this.dataPrevistaSaida = dataPrevistaSaida;
	}

	public Date getDataPrevistaRetorno() {
		return dataPrevistaRetorno;
	}

	public void setDataPrevistaRetorno(Date dataPrevistaRetorno) {
		this.dataPrevistaRetorno = dataPrevistaRetorno;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}

	public EnumStatusOrdemTrafego getStatusSelecionadoOT() {
		return statusSelecionadoOT;
	}

	public void setStatusSelecionadoOT(EnumStatusOrdemTrafego statusSelecionadoOT) {
		this.statusSelecionadoOT = statusSelecionadoOT;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Veiculo getVeiculoPesq() {
		return veiculoPesq;
	}

	public void setVeiculoPesq(Veiculo veiculoPesq) {
		this.veiculoPesq = veiculoPesq;
	}

	public RotaMunicipios getRotaMunicipios() {
		return rotaMunicipios;
	}

	public void setRotaMunicipios(RotaMunicipios rotaMunicipios) {
		this.rotaMunicipios = rotaMunicipios;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}


}
