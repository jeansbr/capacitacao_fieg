package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.ambientjsf.util.UtilFacesRelatorio;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.util.UtilData;
import br.com.ambientinformatica.util.UtilException;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.entidade.EnumStatusDiaria;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.entidade.PrestacaoContaDocumento;
import br.gov.go.semarh.ose.negocio.DiariaNeg;

import br.gov.go.semarh.ose.persistencia.MunicipioDao;

@Controller("PrestacaoContaControl")
@Scope("conversation")
public class PrestacaoContaControl extends Controle {

	@Autowired
	private DiariaNeg diariaNeg;

	private List<Diaria> diarias = new ArrayList<Diaria>();

	private Diaria diaria = new Diaria();

	private Diaria diariaImprimir = new Diaria();

	private Integer numeroDiaria = 0; 

	private EnumStatusDiaria statusDiaria = EnumStatusDiaria.EM_ATENDIMENTO;

	private String descricaoAtividadesRealizadas;

	private Date dataInicial = UtilData.getDataInicioMes(new Date());

	private Date dataFinal = UtilData.getDataFimMes(new Date());

	private DiariaUtilizada diariaUtilizada = new DiariaUtilizada();

	private Funcionario servidor;

	private EnumUf uf = EnumUf.GO;
	
	@Autowired
	private UsuarioLogadoControl usuarioLogadoControl;

	@Autowired
	private MunicipioDao municipioDao;

	private Municipio municipio = new Municipio();

	private List<SelectItem> municipios = new ArrayList<SelectItem>();
	
	private List<PrestacaoContaDocumento> documentos = new ArrayList<PrestacaoContaDocumento>();

	@PostConstruct
	public void init(){
		servidor = usuarioLogadoControl.getFuncionario();
		listar(null);
		atualizarMunicipios();
	}

	public void atualizarMunicipios(){
		try {
			municipios.clear();
			List<Municipio> municipiosList = municipioDao.listarPorUf(uf);
			for(Municipio m : municipiosList){
				municipios.add(new SelectItem(m, m.getDescricao()));
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void addMunicipioPC(ActionEvent evt){
		if(municipio !=null){
			diariaUtilizada.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}
	}

	public void removeMunicipioPC(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		diariaUtilizada.removeMunicipios(municipio);
		municipio = new Municipio();
	}

	public void uploadDocumento(FileUploadEvent event) {
		if(event.getFile() != null){
			PrestacaoContaDocumento documento = new PrestacaoContaDocumento();
			documento.setNomeDocumento(event.getFile().getFileName());
			documento.setDocumento(event.getFile().getContents());
			documentos.add(documento);
			UtilFaces.addMensagemFaces("Upload realizado com sucesso.");
		}else{
			UtilFaces.addMensagemFaces("Selecione um arquivo para realizar o upload.");
		}

	}  
	public void removeDocumento(ActionEvent evt){
		PrestacaoContaDocumento documento = new PrestacaoContaDocumento();
		documento = (PrestacaoContaDocumento) UtilFaces.getValorParametro(evt, "documentoRemover");
		if(documentos.contains(documento)){
			documentos.remove(documento);
		}
	}
	
	public void listar(ActionEvent evt){
		diaria = new Diaria();
		diarias.clear();
		try {
			if(numeroDiaria > 0){
				Diaria diaria = diariaNeg.consultar(numeroDiaria);
				if(diaria.getServidor() == servidor){
					diarias.add(diaria);
				}else{
					UtilFaces.addMensagemFaces("O numero da diaria informada pertence a outro Servidor.");
				}

			}else{
				diarias = diariaNeg.listar(dataInicial, dataFinal, servidor, statusDiaria, null );
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}   

	public void replicarDiariaSolicitada(ActionEvent evt){
		diaria = new Diaria();
		diaria = (Diaria) UtilFaces.getValorParametro(evt, "diariaPrestarConta");	
		diariaUtilizada.setAcrescimo(diaria.getDiariaSolicitada().isAcrescimo());
		diariaUtilizada.setHospedagem(diaria.getDiariaSolicitada().isHospedagem());
		diariaUtilizada.setQuantDiariaIntegral(diaria.getDiariaSolicitada().getQuantDiariaIntegral());
		diariaUtilizada.setQuantDiariaMeia(diaria.getDiariaSolicitada().getQuantDiariaMeia());
		diariaUtilizada.setTipoDiaria(diaria.getDiariaSolicitada().getTipoDiaria());
		diariaUtilizada.setTresQuartoDiaria(diaria.getDiariaSolicitada().getTresQuartoDiaria());
		diariaUtilizada.setUmQuartoDiaria(diaria.getDiariaSolicitada().getUmQuartoDiaria());
		diariaUtilizada.setValorAdotado(diaria.getDiariaSolicitada().getValorAdotado());
		descricaoAtividadesRealizadas = diaria.getDescricaoObjetivoViagem();
		diariaUtilizada.setDataInicio(diaria.getDataInicio());
		diariaUtilizada.setDataFim(diaria.getDataFim());
		diariaUtilizada.getMunicipios().clear();
		for(Municipio municipio : diaria.getMunicipios()){
			diariaUtilizada.addMunicipio(municipio);
		}

	}

	public void encerrar(ActionEvent evt){
		try{
			diariaUtilizada.setDocumentos(documentos);
			diariaUtilizada.setFuncionarioResponsavel(servidor);
			diaria.encerrar(descricaoAtividadesRealizadas, diariaUtilizada);
			diariaNeg.alterar(diaria);
			diaria = new Diaria();
			descricaoAtividadesRealizadas = null;
		} catch(Exception e){
			UtilFaces.addMensagemFaces(e);
		}
	}  



	public void imprimirPrestarConta(){
		try {
			if(diariaImprimir != null){
				diariaImprimir = diariaNeg.consultar(diariaImprimir.getId());
				Calendar cFim = Calendar.getInstance();
				cFim.setTime(diariaImprimir.getDataFim());
				cFim.add(Calendar.DAY_OF_MONTH, 1);
				int diaSemana = cFim.get(Calendar.DAY_OF_WEEK);
				if(diaSemana == Calendar.SATURDAY) {
					cFim.add(Calendar.DAY_OF_MONTH, 2);
				} else if (diaSemana == Calendar.SUNDAY) {
					cFim.add(Calendar.DAY_OF_MONTH, 1);
				}
				Date dataRegistro = cFim.getTime();
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("diaria", diariaImprimir);
				parametros.put("dataRegistro", dataRegistro);
				parametros.put("saldo", diariaImprimir.getDiariaSolicitada().getValorTotal().subtract(diariaImprimir.getDiariaUtilizada().getValorTotal()));

				UtilFacesRelatorio.gerarRelatorioFaces("jasper/prestacaoContaDiaria.jasper", diariaImprimir.getMunicipios(), parametros);
			}
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		} catch (UtilException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public List<SelectItem> getStatusDiarias(){
		return UtilFaces.getListEnum(EnumStatusDiaria.values());
	}

	public List<SelectItem> getUfs(){
		return UtilFaces.getListEnum(EnumUf.values());
	}

	public List<Diaria> getDiarias() {
		return diarias;
	}

	public void setDiarias(List<Diaria> diarias) {
		this.diarias = diarias;
	}

	public Diaria getDiaria() {
		return diaria;
	}

	public void setDiaria(Diaria diaria) {
		this.diaria = diaria;
	}

	public Diaria getDiariaImprimir() {
		return diariaImprimir;
	}

	public void setDiariaImprimir(Diaria diariaImprimir) {
		this.diariaImprimir = diariaImprimir;
	}

	public Integer getNumeroDiaria() {
		return numeroDiaria;
	}

	public void setNumeroDiaria(Integer numeroDiaria) {
		this.numeroDiaria = numeroDiaria;
	}

	public EnumStatusDiaria getStatusDiaria() {
		return statusDiaria;
	}

	public void setStatusDiaria(EnumStatusDiaria statusDiaria) {
		this.statusDiaria = statusDiaria;
	}

	public String getDescricaoAtividadesRealizadas() {
		return descricaoAtividadesRealizadas;
	}

	public void setDescricaoAtividadesRealizadas(
			String descricaoAtividadesRealizadas) {
		this.descricaoAtividadesRealizadas = descricaoAtividadesRealizadas;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public DiariaUtilizada getDiariaUtilizada() {
		return diariaUtilizada;
	}

	public void setDiariaUtilizada(DiariaUtilizada diariaUtilizada) {
		this.diariaUtilizada = diariaUtilizada;
	}

	public Funcionario getServidor() {
		return servidor;
	}

	public void setServidor(Funcionario servidor) {
		this.servidor = servidor;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<SelectItem> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<SelectItem> municipios) {
		this.municipios = municipios;
	}

	public List<PrestacaoContaDocumento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<PrestacaoContaDocumento> documentos) {
		this.documentos = documentos;
	}

}
