package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.negocio.RotaMunicipiosNeg;
import br.gov.go.semarh.ose.persistencia.MunicipioDao;

@Controller("RotaMunicipiosControl")
@Scope("conversation")
public class RotaMunicipiosControl {


	private RotaMunicipios rotaMunicipios = new RotaMunicipios();

	private List<RotaMunicipios> rotaMunicipiosList;

	@Autowired
	private MunicipioDao municipioDao;
	
	@Autowired
	private RotaMunicipiosNeg rotaMunicipiosNeg;

	private Municipio municipio = new Municipio();
	
	private Municipio removeMunicipio;
	
	private List<SelectItem> municipios = new ArrayList<SelectItem>();
	
	private String descricao;
	
	private Integer id;

	private EnumUf uf = EnumUf.GO;

	@PostConstruct
	public void init(){
		listar(null);
		atualizarMunicipios();
	}

	public void atualizarMunicipios(){
		try {
			municipios.clear();
			List<Municipio> municipiosList = municipioDao.listarPorUf(uf);
			for(Municipio m : municipiosList){
				municipios.add(new SelectItem(m, m.getDescricao()));
			}
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	public List<SelectItem> getUfs(){
		return UtilFaces.getListEnum(EnumUf.values());
	}
	
	public void listar(ActionEvent evt){
		try {
			rotaMunicipiosList = rotaMunicipiosNeg.listar(id, descricao);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	public void limpar(){
		rotaMunicipios = new RotaMunicipios();
	}


	public void addMunicipio(ActionEvent evt){
		if(municipio !=null){
			rotaMunicipios.addMunicipio(municipio);
		}else{
			UtilFaces.addMensagemFaces("Selecione um município.");
		}
	}

	public void removeMunicipio(ActionEvent evt){
		municipio = new Municipio();
		municipio = (Municipio) UtilFaces.getValorParametro(evt, "municipioRemover");
		rotaMunicipios.removeMunicipios(municipio);
		municipio = new Municipio();
	}
	
	public void confirmar(ActionEvent evt){
	      try {
	    	  if(rotaMunicipiosNeg.consultarPorDescricao(rotaMunicipios.getDescricao())==null){
	    		  rotaMunicipiosNeg.alterar(rotaMunicipios);
	    		  UtilFaces.addMensagemFaces("Rota salva com sucesso");    		  
	    	  }else{
	    		  UtilFaces.addMensagemFaces("Já existe um serviço com a mesma descrição");
	    	  }
	    	  
	         listar(null);
	         rotaMunicipios = new RotaMunicipios();
	      } catch (Exception e) {
	         UtilFaces.addMensagemFaces(e);
	      }
	   }

	public List<RotaMunicipios> getRotaMunicipiosList() {
		return rotaMunicipiosList;
	}

	public void setRotaMunicipiosList(List<RotaMunicipios> rotaMunicipiosList) {
		this.rotaMunicipiosList = rotaMunicipiosList;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RotaMunicipios getRotaMunicipios() {
		return rotaMunicipios;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}
	
	public Municipio getRemoveMunicipio() {
		return removeMunicipio;
	}
	
	public void setRemoveMunicipio(Municipio removeMunicipio) {
		this.removeMunicipio = removeMunicipio;
	}
	
	public List<SelectItem> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<SelectItem> municipios) {
		this.municipios = municipios;
	}

	public void setRotaMunicipios(RotaMunicipios rotaMunicipios) {
		if(rotaMunicipios != null){
			try {
				rotaMunicipios = rotaMunicipiosNeg.consultar(rotaMunicipios.getId());
			} catch (Exception e) {
				UtilFaces.addMensagemFaces(e);
			}
		}
		this.rotaMunicipios = rotaMunicipios;
	}
	
}
