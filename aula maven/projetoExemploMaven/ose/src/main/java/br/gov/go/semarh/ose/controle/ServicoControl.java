package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.negocio.ServicoNeg;

@Controller("ServicoControl")
@Scope("conversation")
public class ServicoControl extends Controle {

   private Servico servico = new Servico();
   
   private Servico servicoExcluir;

   private List<Servico> servicos = new ArrayList<Servico>();
   
   private Integer id;
   
   private String descricao;
   
   @Autowired
   private ServicoNeg servicoNeg;
   
   @PostConstruct
   public void init(){
      listar(null);
   }
   
   public void confirmar(ActionEvent evt){
      try {
    	  if(servicoNeg.consultarPorDescricao(servico.getDescricao())==null){
    		  servicoNeg.alterar(servico);
    		  UtilFaces.addMensagemFaces("Servico salvo com sucesso");    		  
    	  }else{
    		  UtilFaces.addMensagemFaces("Já existe um serviço com a mesma descrição");
    	  }
    	  
         listar(null);
         servico = new Servico();
      } catch (Exception e) {
         UtilFaces.addMensagemFaces(e);
      }
   }


   public void listar(ActionEvent evt){
      try {
         servicos = servicoNeg.listar( id, descricao);
      } catch (Exception e) {
         UtilFaces.addMensagemFaces(e);
      }
   }

   public void limpar(ActionEvent evt){
	   servico = new Servico();
   }

   public void excluir(ActionEvent evt){
      try {
         if(servicoExcluir != null){
            servicoNeg.excluirPorId(servicoExcluir.getId());
         }
         listar(evt);
      } catch (Exception e) {
         UtilFaces.addMensagemFaces(e);
      }
   }


   public List<Servico> getServicos() {
      return servicos;
   }

   public Servico getServico() {
      return servico;
   }

   public void setServico(Servico servico) {
      this.servico = servico;
   }

   public Servico getServicoExcluir() {
      return servicoExcluir;
   }

   public void setServicoExcluir(Servico servicoExcluir) {
      this.servicoExcluir = servicoExcluir;
   }
   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getDescricao() {
      return descricao;
   }

   public void setDescricao(String descricao) {
      this.descricao = descricao;
   }

}
