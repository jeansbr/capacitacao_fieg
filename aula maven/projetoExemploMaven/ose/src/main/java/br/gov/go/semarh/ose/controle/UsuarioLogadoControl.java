package br.gov.go.semarh.ose.controle;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.persistencia.FuncionarioDao;

@Controller("UsuarioLogadoControl")
@Scope("conversation")
public class UsuarioLogadoControl {

	private Funcionario funcionario;
	
	@Autowired
	private FuncionarioDao funcionarioDao;
	
	@PostConstruct
	public void init(){
		try {
			String email = UtilFaces.getRequest().getUserPrincipal().getName();
			funcionario = funcionarioDao.consultarPorUsuario(email);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}
	
}
