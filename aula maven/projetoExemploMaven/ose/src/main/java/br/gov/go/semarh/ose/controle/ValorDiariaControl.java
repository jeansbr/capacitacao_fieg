package br.gov.go.semarh.ose.controle;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.negocio.ValorDiariaNeg;

@Controller("ValorDiariaControl")
@Scope("conversation")
public class ValorDiariaControl extends Controle{

	private ValorDiaria valorDiaria = new ValorDiaria();

	private ValorDiaria valorDiariaSalva;

	@Autowired
	private ValorDiariaNeg valorDiariaNeg;
	
	@Autowired	
	private UsuarioLogadoControl usuarioLogadoControl;
	

	@PostConstruct
	public void init(){	
		try {
			valorDiariaSalva = valorDiariaNeg.consultarAtivo();
		} catch (PersistenciaException e) {
			UtilFaces.addMensagemFaces(e);
		}

	}

	public void confirmar(ActionEvent evt){
		try {
			if(valorDiariaSalva != null){
				valorDiariaSalva.setDataAlteracao(new Date());
				valorDiariaSalva.setFuncionarioAlteracao(usuarioLogadoControl.getFuncionario());
				valorDiariaSalva.setAtivo(false);
				valorDiariaNeg.alterar(valorDiariaSalva);
			}
			valorDiaria.setFuncionarioRegistro(usuarioLogadoControl.getFuncionario());
			valorDiariaNeg.alterar(valorDiaria);
			
		} catch (PersistenciaException e) {			
			UtilFaces.addMensagemFaces(e);
		}

	}

	public ValorDiaria getValorDiaria() {
		return valorDiaria;
	}
	
	public void setValorDiaria(ValorDiaria valorDiaria) {
		this.valorDiaria = valorDiaria;
	}
	
	public ValorDiaria getValorDiariaSalva() {
		return valorDiariaSalva;
	}
	
	public void setValorDiariaSalva(ValorDiaria valorDiariaSalva) {
		this.valorDiariaSalva = valorDiariaSalva;
	}
	
}
