package br.gov.go.semarh.ose.controle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.ambientinformatica.ambientjsf.controle.Controle;
import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.gov.go.semarh.ose.entidade.EnumStatusVeiculo;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.HistoricoHodometro;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.negocio.HistoricoHodometroNeg;
import br.gov.go.semarh.ose.negocio.VeiculoNeg;

@Controller("VeiculoControl")
@Scope("conversation")
public class VeiculoControl extends Controle{

	private Veiculo veiculo = new Veiculo();

	private Veiculo veiculoAlterar = new Veiculo();

	private List<Veiculo> veiculos = new ArrayList<Veiculo>();

	private String placaConsulta;

	private Integer novoValorHodometro;

	private Funcionario requisitante = new Funcionario();

	@Autowired
	private VeiculoNeg veiculoNeg;
	
	private boolean veiculoEditar = false;

	@Autowired
	private HistoricoHodometroNeg historicoHodometroNeg;

	@Autowired
	private UsuarioLogadoControl usuarioLogadoControl;

	@PostConstruct
	public void init(){
		listar(null);
		requisitante = usuarioLogadoControl.getFuncionario();
	}

	public void confirmar(ActionEvent evt){
		try {
			veiculo.setTipoVeiculo(veiculo.getTipoVeiculo().toUpperCase());
			veiculo.setChassi(veiculo.getChassi().toUpperCase());
			veiculo.setMarcaModelo(veiculo.getMarcaModelo().toUpperCase());
			veiculo.setPlaca(veiculo.getPlaca().toUpperCase());
			if(veiculoNeg.consultarPorPlaca(veiculo.getPlaca())==null || veiculoEditar){
				veiculoNeg.alterar(veiculo);
				veiculoEditar = false;
				UtilFaces.addMensagemFaces("Veiculo salvo com sucesso.");
			}
			else{
				UtilFaces.addMensagemFaces("Placa já cadastrada.");
			}

			veiculo = new Veiculo();
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void listar(ActionEvent evt){
		try {
			if(placaConsulta != null && placaConsulta.length() == 8){
				veiculos = veiculoNeg.listar(null, placaConsulta, null, null, null, null);
			}else{
				veiculos = veiculoNeg.listar(null, null, null, null, null, null);
			}

		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}
	
	public void novoVeiculo(ActionEvent evt){
		veiculo = new Veiculo();
	}
	
	public void editarVeiculo(ActionEvent evt){
		veiculoEditar = true;
	}


	public void alterarHodometro(ActionEvent evt){
		try {
			HistoricoHodometro historicoHodometro = new HistoricoHodometro();
			historicoHodometro.setDataAlteracao(new Date());
			historicoHodometro.setRequisitante(requisitante);
			historicoHodometro.setHodometroAntigo(veiculoAlterar.getHodometro());
			historicoHodometro.setHodometroCorrente(novoValorHodometro);
			historicoHodometroNeg.incluir(historicoHodometro);
			veiculoAlterar.addHistoricoHodometro(historicoHodometro);
			veiculoAlterar.setHodometro(novoValorHodometro);
			veiculoNeg.alterar(veiculoAlterar);
		} catch (Exception e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public List<SelectItem> getStatusVeiculoList(){
		return UtilFaces.getListEnum(EnumStatusVeiculo.values());
	}

	public String getPlacaConsulta() {
		return placaConsulta;
	}

	public void setPlacaConsulta(String placaConsulta) {
		this.placaConsulta = placaConsulta;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public Veiculo getVeiculoAlterar() {
		return veiculoAlterar;
	}

	public void setVeiculoAlterar(Veiculo veiculoAlterar) {
		this.veiculoAlterar = veiculoAlterar;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Integer getNovoValorHodometro() {
		return novoValorHodometro;
	}

	public void setNovoValorHodometro(Integer novoValorHodometro) {
		this.novoValorHodometro = novoValorHodometro;
	}

}
