package br.gov.go.semarh.ose.entidade;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "rh")
public class Cargo {
	
	@Id
	private Integer id;

	private String nome;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

}
