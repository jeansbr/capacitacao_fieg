package br.gov.go.semarh.ose.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="rh")
public class Contrato {
	
	@Id
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="id_cargo")	
	private Cargo cargo;

	@Column(name="dt_inicio")
	@Temporal(TemporalType.DATE)
	private Date dataInicio;

	@Column(name="dt_termino")
	@Temporal(TemporalType.DATE)
	private Date dataTermino;

	public Integer getId() {
		return id;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

}
