package br.gov.go.semarh.ose.entidade;

import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema = "rh")
public class Departamento {

	@Id
	private Integer id;

	private String nome;

	private String sigla;

	private boolean excluido;

	@ManyToOne
	@JoinColumn(name = "id_pai")
	private Departamento departamentoPai;

	@OneToMany
	@JoinColumn(name = "id_pai")
	private List<Departamento> departamentos;

	public String getSigla() {
		return sigla;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public Departamento getDepartamentoPai() {
		return departamentoPai;
	}

	public List<Departamento> getDepartamentos() {
		return Collections.unmodifiableList(departamentos);
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

}
