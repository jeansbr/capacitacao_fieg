package br.gov.go.semarh.ose.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="rh", name="depart_funcionario")
public class DepartamentoFuncionario {

	@Id
	private Integer id;
	
	@Column(name="dt_entrada")
	@Temporal(TemporalType.DATE)
	private Date dataEntrada;

	@Column(name="dt_saida")
	@Temporal(TemporalType.DATE)
	private Date dataSaida;

	@ManyToOne
	@JoinColumn(name="id_depart")
	private Departamento departamento;

	public Integer getId() {
		return id;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public Date getDataSaida() {
		return dataSaida;
	}


	public Departamento getDepartamento() {
		return departamento;
	}
	
}
