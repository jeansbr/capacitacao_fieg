package br.gov.go.semarh.ose.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.gov.go.semarh.ose.util.OseException;

@Entity
public class Diaria {

	@Id
	@GeneratedValue(generator="diaria_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="diaria_seq", sequenceName="diaria_seq", allocationSize=1, initialValue=1)
	private Integer id;

	@Enumerated(EnumType.STRING)
	private EnumStatusDiaria status = EnumStatusDiaria.NOVA;

	@OneToOne
	private Funcionario autoridadeEmitente;

	/**
	 * Para caso de viagem para fora do Estado
	 */

	@OneToOne
	private Funcionario autoridadeTitularOrgao;

	@OneToOne
	private Funcionario servidor;

	@OneToOne
	private Funcionario requisitante;

	@ManyToOne
	private Departamento departamento;

	@ManyToMany
	private List<Municipio> municipios = new ArrayList<Municipio>();

	@Enumerated(EnumType.STRING)
	private EnumTipoTransporte tipoTransporte;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCancelamento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim; 

	/**
	 * Dotacao orcamentaria
	 */

	private String programa;

	private String acao;

	private String fonteDeRecurso;


	@OneToOne(cascade=CascadeType.ALL)
	private DiariaSolicitada diariaSolicitada;

	@OneToOne(cascade=CascadeType.ALL)
	private DiariaUtilizada diariaUtilizada;

	private String descricaoObjetivoViagem;

	private String descricaoAtividadesRealizadas;



	public void atender(Funcionario autoridadeEmitente, Funcionario autoridadeTitularOrgao, Date dataInicio, Date dataFim, DiariaSolicitada diariaSolicitada, String descricaoObjetivoViagem, Funcionario servidor, Funcionario requisitante) throws OseException{
		if(diariaSolicitada == null){
			throw new OseException("É necessário adicionar a(s) diaria(s) solicitada(s)");
		}
		this.status = EnumStatusDiaria.EM_ATENDIMENTO;
		this.autoridadeEmitente = autoridadeEmitente;

		if(autoridadeTitularOrgao != null && autoridadeTitularOrgao.getId() != null){
			this.autoridadeTitularOrgao = autoridadeTitularOrgao;
		}

		this.requisitante = requisitante;
		this.servidor = servidor;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.descricaoObjetivoViagem = descricaoObjetivoViagem;
		this.diariaSolicitada = diariaSolicitada;
	}


	public void novaDiaria(Date dataInicio, Date dataFim, String descricaoObjetivoViagem, Funcionario servidor, Funcionario requisitante, Departamento departamento) throws OseException{

		this.status = EnumStatusDiaria.ABERTA; 
		this.requisitante = requisitante;
		this.servidor = servidor;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.descricaoObjetivoViagem = descricaoObjetivoViagem;
		this.departamento = departamento;

	}

	public void encerrar(String descricaoAtividadesRealizadas, DiariaUtilizada diariaUtilizada){

		this.diariaUtilizada = diariaUtilizada;
		this.status = EnumStatusDiaria.ENCERRADA;
		this.descricaoAtividadesRealizadas = descricaoAtividadesRealizadas;
	}


	public void cancelarDiaria() {
		this.status = EnumStatusDiaria.CANCELADA;
		this.dataCancelamento = new Date();
	}


	public boolean isForaDoEstado(){
		boolean retorno = false;
		if(!municipios.isEmpty()){
			for(Municipio municipio : municipios){
				retorno = municipio.getUf() != EnumUf.GO ? true : false ; 
			}
		}
		return retorno;
	}


	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}


	public void removeMunicipios(Municipio municipio){
		if(municipios.contains(municipio)){
			municipios.remove(municipio);
		}
	}  

	public String getMunicipiosList(){
		String municipiosList = "";
		for(Municipio m : municipios){
			municipiosList = municipiosList.concat(m.getDescricao()).concat("; ");
		}
		return municipiosList;
	}

	public boolean isNova(){
		return status == EnumStatusDiaria.NOVA;
	}

	public boolean isAtender(){
		return status == EnumStatusDiaria.ABERTA;
	}

	public boolean isEncerrar(){
		return status == EnumStatusDiaria.EM_ATENDIMENTO;
	}

	public boolean isEncerrada(){
		return status == EnumStatusDiaria.ENCERRADA;
	}

	public boolean isCancelada(){
		return status == EnumStatusDiaria.CANCELADA;
	}

	public Integer getId() {
		return id;
	}

	public EnumStatusDiaria getStatus() {
		return status;
	}

	public Funcionario getAutoridadeEmitente() {
		return autoridadeEmitente;
	}

	public Funcionario getAutoridadeTitularOrgao() {
		return autoridadeTitularOrgao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public DiariaSolicitada getDiariaSolicitada() {
		return diariaSolicitada;
	}

	public String getDescricaoAtividadesRealizadas() {
		return descricaoAtividadesRealizadas;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public String getDescricaoObjetivoViagem() {
		return descricaoObjetivoViagem;
	}

	public void setDescricaoObjetivoViagem(String descricaoObjetivoViagem) {
		this.descricaoObjetivoViagem = descricaoObjetivoViagem;
	}

	public String getPrograma() {
		return programa;
	}

	public String getAcao() {
		return acao;
	}

	public String getFonteDeRecurso() {
		return fonteDeRecurso;
	}

	public Funcionario getServidor() {
		return servidor;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public EnumTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(EnumTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public DiariaUtilizada getDiariaUtilizada() {
		return diariaUtilizada;
	}


	public Departamento getDepartamento() {
		return departamento;
	}


	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}


	public Date getDataCancelamento() {
		return dataCancelamento;
	}


}
