package br.gov.go.semarh.ose.entidade;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


@Entity
public class DiariaSolicitada {

   @Id
   @GeneratedValue(generator="diariasolicitada_seq", strategy=GenerationType.SEQUENCE)
   @SequenceGenerator(name="diariasolicitada_seq", sequenceName="diariasolicitada_seq", allocationSize=1, initialValue=1)
   private Integer id;
   
   @Enumerated(EnumType.STRING)
   private EnumTipoDiaria tipoDiaria = EnumTipoDiaria.INTERIOR_GO;
   
   private BigDecimal valorAdotado = BigDecimal.ZERO;
   
   private BigDecimal quantDiariaIntegral = BigDecimal.ZERO;
   
   private BigDecimal quantDiariaMeia = BigDecimal.ZERO;
   
   private BigDecimal umQuartoDiaria = BigDecimal.ZERO;
   
   private BigDecimal tresQuartoDiaria = BigDecimal.ZERO;
   
   private boolean hospedagem;
   
   private boolean acrescimo;

   
   
   public BigDecimal getValorTotal(){
	   BigDecimal valorTotal= getValorDiaria();
	   valorTotal= valorTotal.add(getValorDiariaMeia());
	   valorTotal= valorTotal.add(getValorUmQuartoDiaria());
	   valorTotal= valorTotal.add(getValorTresQuartoDiaria());
	   valorTotal= valorTotal.add(getAcrescimoDiaria());
	   valorTotal= valorTotal.add(getAcrescimoDiariaMeia());
	   valorTotal= valorTotal.add(getAcrescimoUmQuartoDiaria());
	   valorTotal= valorTotal.add(getAcrescimoTresQuartoDiaria());
      return  valorTotal.setScale(2);
   }
     
   public BigDecimal getValorDiaria(){
	   if(tipoDiaria == EnumTipoDiaria.REGIAO_METROPOLITANA){
		   return (valorAdotado.multiply(quantDiariaIntegral)).divide(BigDecimal.valueOf(4)).setScale(2);
	   }
	   return valorAdotado.multiply(quantDiariaIntegral).setScale(2);
   }
   
   public BigDecimal getValorDiariaMeia(){
	   if(tipoDiaria == EnumTipoDiaria.REGIAO_METROPOLITANA){
		   return (valorAdotado.multiply(quantDiariaMeia.divide(BigDecimal.valueOf(2)))).divide(BigDecimal.valueOf(4)).setScale(2);
	   }
	   return valorAdotado.multiply(quantDiariaMeia.divide(BigDecimal.valueOf(2))).setScale(2);
   }
   
   public BigDecimal getValorUmQuartoDiaria(){
	   if(tipoDiaria == EnumTipoDiaria.REGIAO_METROPOLITANA){
		   return (valorAdotado.multiply(umQuartoDiaria.divide(BigDecimal.valueOf(4)))).divide(BigDecimal.valueOf(4)).setScale(2);
	   }
	   return valorAdotado.multiply(umQuartoDiaria.divide(BigDecimal.valueOf(4))).setScale(2);
   }
   
   public BigDecimal getValorTresQuartoDiaria(){
	   if(tipoDiaria == EnumTipoDiaria.REGIAO_METROPOLITANA){
		   return valorAdotado.multiply((tresQuartoDiaria.divide(BigDecimal.valueOf(4))).multiply(BigDecimal.valueOf(3))).divide(BigDecimal.valueOf(4)).setScale(2);
	   }
	   return valorAdotado.multiply((tresQuartoDiaria.divide(BigDecimal.valueOf(4))).multiply(BigDecimal.valueOf(3))).setScale(2);
   }
   
   public BigDecimal getAcrescimoDiaria(){
	   if(acrescimo)
		   return getValorDiaria().divide(BigDecimal.valueOf(4)).setScale(2);
	   return BigDecimal.ZERO;
   }
   
   public BigDecimal getAcrescimoDiariaMeia(){
	   if(acrescimo)
		   return getValorDiariaMeia().divide(BigDecimal.valueOf(4)).setScale(2);
	   return BigDecimal.ZERO;
   }
   
   public BigDecimal getAcrescimoUmQuartoDiaria(){
	   if(acrescimo)
		   return getValorUmQuartoDiaria().divide(BigDecimal.valueOf(4)).setScale(2);
	   return BigDecimal.ZERO;
   }
   
   public BigDecimal getAcrescimoTresQuartoDiaria(){
	   if(acrescimo)
		   return getValorTresQuartoDiaria().divide(BigDecimal.valueOf(4)).setScale(2);
	   return BigDecimal.ZERO;
   }
   
   /*
    * Getters and Setters
    */
   public EnumTipoDiaria getTipoDiaria() {
      return tipoDiaria;
   }

   public void setTipoDiaria(EnumTipoDiaria tipoDiaria) {
      this.tipoDiaria = tipoDiaria;
   }

   public boolean isAcrescimo() {
      return acrescimo;
   }

   public void setAcrescimo(boolean acrescimo) {
      this.acrescimo = acrescimo;
   }

   public Integer getId() {
      return id;
   }

   public BigDecimal getValorAdotado() {
	   return valorAdotado;
   }

   public void setValorAdotado(BigDecimal valorAdotado) {
	   this.valorAdotado = valorAdotado;
   }

   public BigDecimal getQuantDiariaIntegral() {
	   return quantDiariaIntegral;
   }

   public void setQuantDiariaIntegral(BigDecimal quantDiariaIntegral) {
	   this.quantDiariaIntegral = quantDiariaIntegral;
   }

   public BigDecimal getQuantDiariaMeia() {
	   return quantDiariaMeia;
   }

   public void setQuantDiariaMeia(BigDecimal quantDiariaMeia) {
	   this.quantDiariaMeia = quantDiariaMeia;
   }

   public boolean isHospedagem() {
	   return hospedagem;
   }

   public void setHospedagem(boolean hospedagem) {
	   this.hospedagem = hospedagem;
   }

   public BigDecimal getUmQuartoDiaria() {
	   return umQuartoDiaria;
   }

   public void setUmQuartoDiaria(BigDecimal umQuartoDiaria) {
	   this.umQuartoDiaria = umQuartoDiaria;
   }

   public BigDecimal getTresQuartoDiaria() {
	   return tresQuartoDiaria;
   }

   public void setTresQuartoDiaria(BigDecimal tresQuartoDiaria) {
	   this.tresQuartoDiaria = tresQuartoDiaria;
   }

}
