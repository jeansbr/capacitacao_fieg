package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumDiarias implements IEnum{

   INTEGRAIS("Diárias Integrais"),
   PARCIAL_A("Diárias parcial A"),
   PARCIAL_B("Diárias parcial B"),
   PARCIAL_C("Diárias parcial C"),
   PARCIAL_D("Diárias parcial D");

   private final String descricao;
   
   private EnumDiarias(String descricao) {
      this.descricao = descricao;
   }
   
   @Override
   public String getDescricao() {
      return descricao;
   }
   
}
