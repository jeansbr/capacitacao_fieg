package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumStatusDiaria implements IEnum {

	NOVA("Nova"),
	ABERTA("Aberta"),
	EM_ATENDIMENTO("Em Atendimento"),
	CANCELADA("Cancelada"),
	ENCERRADA("Encerrada");

	private final String descricao;

	private EnumStatusDiaria(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
