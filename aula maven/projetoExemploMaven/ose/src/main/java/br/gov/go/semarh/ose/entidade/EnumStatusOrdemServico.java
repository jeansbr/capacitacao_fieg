package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumStatusOrdemServico implements IEnum{

   EM_EDICAO ("Aberta - Em edição"),
   GERADA ("Gerada"),
   CANCELADA ("Cancelada");
   
   private final String descricao;
   
   private EnumStatusOrdemServico(String descricao) {
      this.descricao = descricao;
   }

   public String getDescricao() {
      return descricao;
   }
}
