package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumStatusVeiculo implements IEnum{
	LIVRE("Livre"),
	OCUPADO("Ocupado"),
	MANUTENCAO("Em Manutenção"),
	INATIVO("Inativo");
	
	private final String descricao;

	private EnumStatusVeiculo(String descricao){
		this.descricao = descricao;
	}
	public String getDescricao() {
		
		return descricao;
	}

}
