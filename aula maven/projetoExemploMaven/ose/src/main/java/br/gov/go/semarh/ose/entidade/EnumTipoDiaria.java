package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumTipoDiaria implements IEnum{

	INTERIOR_GO("Interior GO"),
	
	REGIAO_METROPOLITANA("Região Metropolitana"),
	
	OUTRAS_UF("Outras UF");
	
	
	private final String descricao;
	
	private EnumTipoDiaria(String descricao) {
			this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
