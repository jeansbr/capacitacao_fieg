package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumTipoDocumentoServico implements IEnum{

   PROCESSO("PROCESSO"),
   
   BO("BO"),
   
   OFICIO("OFICIO");
   
   private final String descricao;
   
   private EnumTipoDocumentoServico(String descricao) {
      this.descricao = descricao;
   }

   public String getDescricao() {
      return descricao;
   }
   
}
