package br.gov.go.semarh.ose.entidade;

import br.com.ambientinformatica.util.IEnum;

public enum EnumTipoTransporte implements IEnum{

   VEICULO_OFICIAL("Veículo Oficial"),
   VEICULO_PROPRIO ("Veículo Próprio");
   
   private final String descricao;
   
   private EnumTipoTransporte(String descricao) {
      this.descricao = descricao;
   }

   public String getDescricao() {
      return descricao;
   }
}
