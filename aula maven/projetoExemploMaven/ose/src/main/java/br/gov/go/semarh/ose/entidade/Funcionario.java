package br.gov.go.semarh.ose.entidade;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(schema="rh")
public class Funcionario {

   @Id
   private Integer id;
  
   private String nome;
   
   private String cpf;
   
   private String endereco;
   
   private String telefone;
  
   private String cnh;
   
   private String matricula;
   
   private String banco;
   
   private String agencia;
  
   private String contaCorrente;
   
   private Boolean ativo;
   
   @ManyToOne
   @JoinColumn(name="id_depart_lotacao_folha_ponto")
   private Departamento lotacao;
   

   @ManyToOne
   @JoinColumn(name="id_usuario")
   private Usuario usuario;
      
   
   @OneToMany
   @JoinColumn(name="id_func")
   private List<Contrato> contratos = new ArrayList<Contrato>();
   
   @OneToMany
   @JoinColumn(name="id_funcionario")
   private List<DepartamentoFuncionario> departamentos = new ArrayList<DepartamentoFuncionario>();  
   
   	
	@Override
	public String toString() {
		return nome + " - " + matricula;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}
	
	
	public Integer getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCnh() {
		return cnh;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getMatricula() {
		return matricula;
	}
	public String getBanco() {
		return banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}
	public List<DepartamentoFuncionario> getDepartamentos() {
		return departamentos;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public Departamento getLotacao() {
		return lotacao;
	}
	
	public Cargo getCargo(){
		Contrato contratoCargo = new Contrato();
		for(Contrato contrato: contratos){
			if(contrato.getDataTermino()==null){
				return contrato.getCargo();
			}
			contratoCargo = contrato;
		}
		return contratoCargo.getCargo();		
	}

}
