package br.gov.go.semarh.ose.entidade;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class HistoricoHodometro {

	@Id
	@GeneratedValue(generator="historicoAlteracaoHodometro_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="historicoAlteracaoHodometro_seq", sequenceName="historicoAlteracaoHodometro_seq", allocationSize=1, initialValue=1)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAlteracao = new Date();

	@OneToOne(fetch=FetchType.LAZY)
	private Funcionario requisitante;

	private Integer hodometroAntigo;

	private Integer hodometroCorrente;

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Integer getHodometroAntigo() {
		return hodometroAntigo;
	}

	public void setHodometroAntigo(Integer hodometroAntigo) {
		this.hodometroAntigo = hodometroAntigo;
	}

	public Integer getHodometroCorrente() {
		return hodometroCorrente;
	}

	public void setHodometroCorrente(Integer hodometroCorrente) {
		this.hodometroCorrente = hodometroCorrente;
	}

	public Integer getId() {
		return id;
	}



}
