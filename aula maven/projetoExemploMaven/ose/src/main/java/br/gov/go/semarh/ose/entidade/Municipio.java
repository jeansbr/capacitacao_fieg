package br.gov.go.semarh.ose.entidade;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import br.com.ambientinformatica.util.UtilTexto;

@Entity
public class Municipio implements Serializable{
   
   private static final long serialVersionUID = 1L;

   @Id
   private Integer id;
 
   private String descricao;

   @Enumerated(EnumType.STRING)
   private EnumUf uf;
   
   private Integer codigoIbge;

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getDescricao() {
      return UtilTexto.removerAcentos(descricao);
   }

   public void setDescricao(String descricao) {
      this.descricao = descricao;
   }

   public EnumUf getUf() {
      return uf;
   }

   public void setUf(EnumUf uf) {
      this.uf = uf;
   }

   public Integer getCodigoIbge() {
      return codigoIbge;
   }

   public void setCodigoIbge(Integer codigoIbge) {
      this.codigoIbge = codigoIbge;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? super.hashCode() : id.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if(obj != null && obj.getClass().equals(getClass())){
         return obj.hashCode() == hashCode();
      }else{
         return false;
      } 
   }

   @Override
   public String toString() {
      return String.format("%s (%s)", descricao, uf);
   }
   
}
 
