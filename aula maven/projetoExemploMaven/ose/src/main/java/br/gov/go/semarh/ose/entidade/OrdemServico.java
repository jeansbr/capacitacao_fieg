package br.gov.go.semarh.ose.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.ambientinformatica.ambientjsf.util.UtilFaces;
import br.gov.go.semarh.ose.util.OseException;

@Entity
public class OrdemServico {

	@Id
	@GeneratedValue(generator="ordem_servico_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="ordem_servico_seq", sequenceName="ordem_servico_seq", allocationSize=1, initialValue=1)
	private Integer id;

	@OneToOne
	private Funcionario requisitante;
	
	@ManyToOne
	private Funcionario condutor;
	
	@ManyToOne
	private Departamento departamento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraInicio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraFim;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCancelamento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@OneToOne(cascade=CascadeType.ALL)
	private OrdemTrafego ordemTrafego;

	@OneToMany
	@JoinColumn(name="ordermservico_id")
	private List<Diaria> diarias = new ArrayList<Diaria>();

	@Enumerated(EnumType.STRING)
	private EnumTipoTransporte tipoTransporte;

	@Enumerated(EnumType.STRING)
	private EnumStatusOrdemServico estadoOrdemServico = EnumStatusOrdemServico.EM_EDICAO;   

	@ManyToMany
	private List<Municipio> municipios = new ArrayList<Municipio>();

	@ManyToMany
	private Set<Funcionario> funcionarios = new HashSet<Funcionario>();

	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="ordemservico_id")
	private Set<ServicoExecutado> servicosExecutados = new HashSet<ServicoExecutado>();

	private boolean necessitaDiaria;
	
	public OrdemServico(Funcionario requisitante){
		this();
		this.requisitante = requisitante;
		this.departamento = requisitante.getLotacao();
	}
	
	private OrdemServico(){
		
	}

	public void confirmarOrdemServico(){
		this.estadoOrdemServico = EnumStatusOrdemServico.GERADA;
	}

	public void cancelarOrdemServico(){
		this.estadoOrdemServico = EnumStatusOrdemServico.CANCELADA;
		this.dataCancelamento =  new Date();
	}


	public void gerarOrdemTrafego(){
		if(ordemTrafego == null){
			ordemTrafego = new OrdemTrafego();
		}
		ordemTrafego.setViagem(true);		
		ordemTrafego.getFuncionarios().clear();
		for(Funcionario funcionario: funcionarios){
			ordemTrafego.addFuncionario(funcionario);
		}
		ordemTrafego.getMunicipios().clear();
		for(Municipio municipio : municipios){
			ordemTrafego.addMunicipio(municipio);
		}
		ordemTrafego.getServicos().clear();
		for(ServicoExecutado servicoExecutado : servicosExecutados){
			ordemTrafego.addServico(servicoExecutado.getServico());
		}
		
		ordemTrafego.setDepartamento(departamento);
		ordemTrafego.setMotorista(condutor);
		ordemTrafego.setDataPrevistaRetorno(dataHoraFim);
		ordemTrafego.setDataPrevistaSaida(dataHoraInicio);
		ordemTrafego.setDataHoraSaida(dataHoraInicio);
		ordemTrafego.setRequisitante(requisitante);
		ordemTrafego.setGeradoPor(requisitante);
	}

	public void gerarDiaria(){
		necessitaDiaria = true;
		try {
			for(Funcionario funcionario : funcionarios){
				Diaria diaria = new Diaria();
				diaria.novaDiaria(dataHoraInicio, dataHoraFim, "Informar objetivo", funcionario, requisitante, departamento);
				for(Municipio m : municipios){
					diaria.addMunicipio(m);
				}
				diaria.setTipoTransporte(this.getTipoTransporte());
				diarias.add(diaria);
			}
		} catch (OseException e) {
			UtilFaces.addMensagemFaces(e);
		}
	}

	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}

	public void removeMunicipios(Municipio municipio){
		if(municipios.contains(municipio)){
			municipios.remove(municipio);
		}
	}  

	public void addFuncionario(Funcionario funcionario){
		if(!funcionarios.contains(funcionario)){
			funcionarios.add(funcionario);
		}
	}

	public void removeFuncionario(Funcionario funcionario){
		if(funcionarios.contains(funcionario)){
			funcionarios.remove(funcionario);
		}
	}   

	public void addServicosExecutados(ServicoExecutado serv){
		if(!servicosExecutados.contains(serv)){
			servicosExecutados.add(serv);
		}
	}

	public void addAllServicosExecutados(List<ServicoExecutado> servicos){
		servicosExecutados.clear();
		servicosExecutados.addAll(servicos);
	}

	public List<Funcionario> getFuncionariosList() {
		return new ArrayList<Funcionario>(funcionarios);
	}

	public List<ServicoExecutado> getServicosExecutadosList() {
		return new ArrayList<ServicoExecutado>(servicosExecutados);
	}

	public boolean getEditavel(){
		return estadoOrdemServico == EnumStatusOrdemServico.EM_EDICAO;
	}

	public boolean isNecessitaDiaria() {
		return necessitaDiaria;
	}
	
	public boolean isImprimivel(){
		return estadoOrdemServico == EnumStatusOrdemServico.GERADA;
	}
	
	public boolean isCancelada(){
		return estadoOrdemServico == EnumStatusOrdemServico.CANCELADA;
	}

	public void setNecessitaDiaria(boolean necessitaDiaria) {
		this.necessitaDiaria = necessitaDiaria;
	}

	public void setNecessitaDiaria(Boolean necessitaDiaria) {
		this.necessitaDiaria = necessitaDiaria;
	}

	public EnumTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(EnumTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Date getDataHoraInicio() {
		return dataHoraInicio;
	}

	public void setDataHoraInicio(Date dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public Date getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Set<ServicoExecutado> getServicosExecutados() {
		return servicosExecutados;
	}

	public void setServicosExecutados(Set<ServicoExecutado> servicosExecutados) {
		this.servicosExecutados = servicosExecutados;
	}

	public OrdemTrafego getOrdemTrafego() {
		return ordemTrafego;
	}

	public void setOrdemTrafego(OrdemTrafego ordemTrafego) {
		this.ordemTrafego = ordemTrafego;
	}

	public Integer getId() {
		return id;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public EnumStatusOrdemServico getEstadoOrdemServico() {
		return estadoOrdemServico;
	}

	public List<Diaria> getDiarias() {
		return diarias;
	}

	public void setDiarias(List<Diaria> diarias) {
		this.diarias = diarias;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Funcionario getCondutor() {
		return condutor;
	}

	public void setCondutor(Funcionario condutor) {
		this.condutor = condutor;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}
	
}
