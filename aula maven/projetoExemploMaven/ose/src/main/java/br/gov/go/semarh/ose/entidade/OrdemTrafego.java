package br.gov.go.semarh.ose.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class OrdemTrafego {

	@Id
	@GeneratedValue(generator = "ordem_trafego_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ordem_trafego_seq", sequenceName = "ordem_trafego_seq", allocationSize = 1, initialValue = 1)
	private Integer id;

	
	@Enumerated(EnumType.STRING)
	private EnumStatusOrdemTrafego status = EnumStatusOrdemTrafego.ABERTA;

	@ManyToOne
	private Veiculo veiculo;

	private Integer hodometroSaida;
	
	private Integer hodometroRetorno;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro = new Date();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCancelamento;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraSaida = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraRetorno;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPrevistaSaida;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPrevistaRetorno;
	
	@ManyToOne
	private Funcionario requisitante;
	
	@ManyToOne
	private Departamento departamento;
	
	@ManyToOne
	private Funcionario geradoPor;
	
	@ManyToOne
	private Funcionario autorizadoPor;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAutorizacao;
	
	private String codigoAutorizacao;


	@ManyToOne
	private Funcionario motorista;
	
	private boolean viagem;
	
	@ManyToMany
	private List<Municipio> municipios = new ArrayList<Municipio>();
	
	@ManyToMany
	private Set<Funcionario> funcionarios = new HashSet<Funcionario>();
	
	@ManyToMany
	private Set<Servico> servicos = new HashSet<Servico>();
	
	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}

	public void removeMunicipios(Municipio municipio){
		municipios.remove(municipio);
	}  

	public void addFuncionario(Funcionario funcionario){
		funcionarios.add(funcionario);
	}

	public void removeFuncionario(Funcionario funcionario){
		funcionarios.remove(funcionario);
	}
	
	public void addServico(Servico serv){
		servicos.add(serv);
	}

	public List<Servico> getServicosList() {
		return new ArrayList<Servico>(servicos);
	}

	
	public void confirmarOT(Veiculo veiculo, Date dataHoraSaida, Funcionario motorista, Funcionario requerente, Boolean viagem, Date dataPrevistaRetorno, Funcionario progenitor) {
		
		if(this.status == EnumStatusOrdemTrafego.NOVA){
			this.viagem = viagem;
			this.dataPrevistaSaida = dataHoraSaida;
			this.dataPrevistaRetorno = dataPrevistaRetorno;
			this.requisitante = requerente;
			this.geradoPor = progenitor;
		}
		
		this.dataHoraSaida = dataHoraSaida;
		
		this.veiculo = veiculo;
		this.hodometroSaida = veiculo.getHodometro();		
		this.motorista = motorista;		
		
		this.status = EnumStatusOrdemTrafego.EM_ATENDIMENTO;
		
	}
	
	public void desatender(){
		this.dataHoraSaida = this.dataPrevistaSaida;
		this.veiculo = null;
		this.hodometroSaida = null;		
		this.motorista = null;
		
		this.status = EnumStatusOrdemTrafego.ABERTA;
		
	}

	public void encerrar(Integer hodometro, Date dataHoraRetorno) {
		this.status = EnumStatusOrdemTrafego.ENCERRADA;
		this.dataHoraRetorno = dataHoraRetorno;
		this.hodometroRetorno = hodometro;
	}
	
	public void desencerrar(){
		this.dataHoraRetorno = null;
		this.hodometroRetorno = null;
		this.status = EnumStatusOrdemTrafego.EM_ATENDIMENTO;
	}
	
	public void autorizar(Funcionario autorizador, String codigoAutorizacao){
		this.autorizadoPor = autorizador;
		this.dataAutorizacao = new Date();
		this.codigoAutorizacao = codigoAutorizacao;
	}
		
	public void novaOT() {
		this.status = EnumStatusOrdemTrafego.NOVA;
	}

	public void cancelarOrdemTrafego() {
		this.status = EnumStatusOrdemTrafego.CANCELADA;
		this.dataCancelamento = new Date();
	}

	public boolean isNova() {
		return status == EnumStatusOrdemTrafego.NOVA;
	}
	
	public boolean isAtender() {
		return status == EnumStatusOrdemTrafego.ABERTA;
	}

	public boolean isEncerrar() {
		return status == EnumStatusOrdemTrafego.EM_ATENDIMENTO;
	}
	
	public boolean isEncerrada() {
		return status == EnumStatusOrdemTrafego.ENCERRADA;
	}
	
	public boolean isCancelada() {
		return status == EnumStatusOrdemTrafego.CANCELADA;
	}
	
	public String getMunicipiosList(){
		String municipiosList = "";
		for(Municipio m : municipios){
			municipiosList = municipiosList.concat(m.getDescricao()).concat("; ");
		}
		return municipiosList;
	}
	
	public String getFuncionariosTransportados(){
		String funcionariosTransportados = "";
		for(Funcionario f : funcionarios){
			funcionariosTransportados = funcionariosTransportados.concat(f.getNome()).concat("; ");
		}
		return funcionariosTransportados;
	}
	
	public String getListaServicos(){
		String servicosList = "";
		for(Servico s : servicos){
			servicosList = servicosList.concat(s.getDescricao()).concat("; ");
		}
		return servicosList;
	}
		
	public List<Funcionario> getFuncionariosList() {
		return new ArrayList<Funcionario>(funcionarios);
	}


	public Integer getId() {
		return id;
	}

	public EnumStatusOrdemTrafego getStatus() {
		return status;
	}

	public Integer getHodometroRetorno() {
		return hodometroRetorno;
	}

	public Date getDataHoraSaida() {
		return dataHoraSaida;
	}

	public Date getDataHoraRetorno() {
		return dataHoraRetorno;
	}

	public Funcionario getMotorista() {
		return motorista;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Integer getHodometroSaida() {
		return hodometroSaida;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public Funcionario getRequisitante() {
		return requisitante;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}	

	public Date getDataPrevistaSaida() {
		return dataPrevistaSaida;
	}

	public void setDataPrevistaSaida(Date dataPrevistaSaida) {
		this.dataPrevistaSaida = dataPrevistaSaida;
	}

	public Date getDataPrevistaRetorno() {
		return dataPrevistaRetorno;
	}

	public void setDataPrevistaRetorno(Date dataPrevistaRetorno) {
		this.dataPrevistaRetorno = dataPrevistaRetorno;
	}

	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public Set<Servico> getServicos() {
		return servicos;
	}

	public boolean isViagem() {
		return viagem;
	}

	public void setViagem(boolean viagem) {
		this.viagem = viagem;
	}

	public void setRequisitante(Funcionario requisitante) {
		this.requisitante = requisitante;
	}

	public Funcionario getGeradoPor() {
		return geradoPor;
	}

	public void setGeradoPor(Funcionario geradoPor) {
		this.geradoPor = geradoPor;
	}

	public void setDataHoraSaida(Date dataHoraSaida) {
		this.dataHoraSaida = dataHoraSaida;
	}
	
	public Funcionario getAutorizadoPor() {
		return autorizadoPor;
	}
	
	public Date getDataAutorizacao() {
		return dataAutorizacao;
	}

	public String getCodigoAutorizacao() {
		return codigoAutorizacao;
	}

	public void setMotorista(Funcionario motorista) {
		this.motorista = motorista;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}
		
}
