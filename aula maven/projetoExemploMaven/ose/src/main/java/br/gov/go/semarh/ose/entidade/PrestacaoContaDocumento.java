package br.gov.go.semarh.ose.entidade;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@Entity
public class PrestacaoContaDocumento {
	
	@Id
	@GeneratedValue(generator="prestacaoContaDocumento_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="prestacaoContaDocumento_seq", sequenceName="prestacaoContaDocumento_seq", allocationSize=1, initialValue=1)
	private Integer id;
	
	private String nomeDocumento;
	
	@Lob
	private byte [] documento;

	public byte[] getDocumento() {
		return documento;
	}
	
	public StreamedContent getDocumentoDownload(){
		InputStream stream = new ByteArrayInputStream(documento);
		StreamedContent file =  new DefaultStreamedContent(stream, null, nomeDocumento);
		return file;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	public Integer getId() {
		return id;
	}

	public String getNomeDocumento() {
		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}
	
	
}
