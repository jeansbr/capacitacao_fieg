package br.gov.go.semarh.ose.entidade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class RotaMunicipios{


	@Id
	@GeneratedValue(generator="rota_municipios_seq", strategy= GenerationType.SEQUENCE)
	@SequenceGenerator(name="rota_municipios_seq", sequenceName="rota_municipios_seq", allocationSize=1, initialValue=1)
	private Integer id;

	private String descricao;

	@ManyToMany
	private List<Municipio> municipios = new ArrayList<Municipio>();

	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}


	public void removeMunicipios(Municipio municipio){
		if(municipios.contains(municipio)){
			municipios.remove(municipio);
		}
	}  

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}


}
