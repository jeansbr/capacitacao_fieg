package br.gov.go.semarh.ose.entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Servico {

   @Id
   @GeneratedValue(generator="servico_seq", strategy=GenerationType.SEQUENCE)
   @SequenceGenerator(name="servico_seq", sequenceName="servico_seq", allocationSize=1, initialValue=1)
   private Integer id;
   
   private String descricao;
   
   public Integer getId() {
      return id;
   }

   public String getDescricao() {
      return descricao;
   }

   public void setDescricao(String descricao) {
      this.descricao = descricao;
   }

   @Override
   public int hashCode() {
	   final int prime = 31;
	   int result = 1;
	   result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
	   return result;
   }

   @Override
   public boolean equals(Object obj) {
	   if (this == obj)
		   return true;
	   if (obj == null)
		   return false;
	   if (getClass() != obj.getClass())
		   return false;
	   Servico other = (Servico) obj;
	   if (descricao == null) {
		   if (other.descricao != null)
			   return false;
	   } else if (!descricao.equals(other.descricao))
		   return false;
	   return true;
   }
   
}
