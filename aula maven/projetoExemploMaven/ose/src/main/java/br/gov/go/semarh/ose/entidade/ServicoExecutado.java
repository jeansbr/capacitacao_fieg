package br.gov.go.semarh.ose.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ServicoExecutado {

   @Id
   @GeneratedValue(generator="servico_executado_seq", strategy=GenerationType.SEQUENCE)
   @SequenceGenerator(name="servico_executado_seq", sequenceName="servico_executado_seq", allocationSize=1, initialValue=1)
   private Integer id;
   
   @ManyToOne
   private Servico servico;
   
   private String numeroDocumento;

   @Enumerated(EnumType.STRING)
   private EnumTipoDocumentoServico tipoDocumento = EnumTipoDocumentoServico.PROCESSO;

   @Column(length=5000)
   private String observacao;
   
   /*
    * Nao colocar valor(r$) dos autos,  Quantidade de autos, Relatorio
    * Pois sao campos que pertencem ao Auto de Infração e não a Ordem de Serviço
    */

   public Servico getServico() {
      return servico;
   }

   public void setServico(Servico servico) {
      this.servico = servico;
   }

   public String getNumeroDocumento() {
      return numeroDocumento;
   }


   @Override
   public int hashCode() {
	   final int prime = 31;
	   int result = 1;
	   result = prime * result
			   + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
	   result = prime * result + ((servico == null) ? 0 : servico.hashCode());
	   result = prime * result
			   + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
	   return result;
   }

   @Override
   public boolean equals(Object obj) {
	   if (this == obj)
		   return true;
	   if (obj == null)
		   return false;
	   if (getClass() != obj.getClass())
		   return false;
	   ServicoExecutado other = (ServicoExecutado) obj;
	   if (numeroDocumento == null) {
		   if (other.numeroDocumento != null)
			   return false;
	   } else if (!numeroDocumento.equals(other.numeroDocumento))
		   return false;
	   if (servico == null) {
		   if (other.servico != null)
			   return false;
	   } else if (!servico.equals(other.servico))
		   return false;
	   if (tipoDocumento != other.tipoDocumento)
		   return false;
	   return true;
   }

   public void setNumeroDocumento(String numeroDocumento) {
	   this.numeroDocumento = numeroDocumento;
   }

   public EnumTipoDocumentoServico getTipoDocumento() {
      return tipoDocumento;
   }

   public void setTipoDocumento(EnumTipoDocumentoServico tipoDocumento) {
      this.tipoDocumento = tipoDocumento;
   }

   public String getObservacao() {
      return observacao;
   }

   public void setObservacao(String observacao) {
      this.observacao = observacao;
   }

   public Integer getId() {
      return id;
   }
   
}
