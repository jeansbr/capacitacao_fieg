package br.gov.go.semarh.ose.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(schema = "acesso")
public class Usuario {
	@Id
	@GeneratedValue(generator = "usuario_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "usuario_id_seq", sequenceName = "acesso.usuario_id_seq", allocationSize = 1, initialValue = 1)
	private Integer id;

	private String nome;

	private String email;

	@Column(name = "dt_cadastro")
	private Date dataCadastro;

	@Column(name = "dt_ult_acesso")
	private Date dataUltimoAcesso;

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public Date getDataUltimoAcesso() {
		return dataUltimoAcesso;
	}

	public Integer getId() {
		return id;
	}


}
