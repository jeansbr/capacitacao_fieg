package br.gov.go.semarh.ose.entidade;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ValorDiaria {

	@Id
	@GeneratedValue(generator = "valorDiaria_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="valorDiaria_seq", sequenceName="valorDiaria_seq", allocationSize=1, initialValue=1)
	private Integer id;
	
	private BigDecimal valorDiariaEstado;
	
	private BigDecimal valorDiariaForaEstado;
	
	private boolean ativo = true;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro = new Date();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAlteracao;
	
	@OneToOne
	private Funcionario funcionarioRegistro;
	
	@OneToOne
	private Funcionario funcionarioAlteracao;

	public BigDecimal getValorDiariaEstado() {
		return valorDiariaEstado;
	}

	public void setValorDiariaEstado(BigDecimal valorDiariaEstado) {
		this.valorDiariaEstado = valorDiariaEstado;
	}

	public BigDecimal getValorDiariaForaEstado() {
		return valorDiariaForaEstado;
	}

	public void setValorDiariaForaEstado(BigDecimal valorDiariaForaEstado) {
		this.valorDiariaForaEstado = valorDiariaForaEstado;
	}

	public Integer getId() {
		return id;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Funcionario getFuncionarioRegistro() {
		return funcionarioRegistro;
	}

	public void setFuncionarioRegistro(Funcionario funcionarioRegistro) {
		this.funcionarioRegistro = funcionarioRegistro;
	}

	public Funcionario getFuncionarioAlteracao() {
		return funcionarioAlteracao;
	}

	public void setFuncionarioAlteracao(Funcionario funcionarioAlteracao) {
		this.funcionarioAlteracao = funcionarioAlteracao;
	}
	
	
}
