package br.gov.go.semarh.ose.entidade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Veiculo {

	@Id
	@GeneratedValue(generator="veiculo_seq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="veiculo_seq", sequenceName="veiculo_seq", allocationSize=1, initialValue=1)
	private Integer id;

	private String placa;

	private String marcaModelo;
	
	private String chassi;

	private String tipoVeiculo;

	private Integer hodometro;
	
	private EnumStatusVeiculo statusVeiculo = EnumStatusVeiculo.LIVRE;

	@OneToMany
	@JoinColumn(name="veiculo_id")
	private List<HistoricoHodometro> historicoOdometro = new ArrayList<HistoricoHodometro>();
	
	public void addHistoricoHodometro(HistoricoHodometro historicoHodometro){
	   historicoOdometro.add(historicoHodometro);
	}
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getHodometro() {
		return hodometro;
	}

	public void setHodometro(Integer hodometro) {
		this.hodometro = hodometro;
	}

	public String getMarcaModelo() {
		return marcaModelo;
	}

	public void setMarcaModelo(String marcaModelo) {
		this.marcaModelo = marcaModelo;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(String tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Integer getId() {
		return id;
	}

	public List<HistoricoHodometro> getHistoricoHodometro() {
		return historicoOdometro;
	}

	public EnumStatusVeiculo getStatusVeiculo() {
		return statusVeiculo;
	}

	public void setStatusVeiculo(EnumStatusVeiculo statusVeiculo) {
		this.statusVeiculo = statusVeiculo;
	}
	
   
   
}
