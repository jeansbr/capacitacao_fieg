package br.gov.go.semarh.ose.negocio;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.util.OseException;

public interface DepartamentoNeg extends Negocio<Departamento>{

	public Departamento consultarPorNome(String nome) throws OseException;

	public List<Departamento> listar(Integer id, String nome) throws PersistenciaException;
	

}
