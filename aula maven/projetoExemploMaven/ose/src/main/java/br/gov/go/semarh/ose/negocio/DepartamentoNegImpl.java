package br.gov.go.semarh.ose.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.persistencia.DepartamentoDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("departamentoNeg")
public class DepartamentoNegImpl extends NegocioGenerico<Departamento> implements DepartamentoNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	public DepartamentoNegImpl(DepartamentoDao departamentoDao) {
		super(departamentoDao);
	}

	@Override
	public Departamento consultarPorNome(String nome) throws OseException {
		return ((DepartamentoDao)persistencia).consultarPorNome(nome);
	}

	@Override
	public List<Departamento> listar(Integer id, String nome) throws PersistenciaException {
		return ((DepartamentoDao)persistencia).listar(id, nome);
	}

}
