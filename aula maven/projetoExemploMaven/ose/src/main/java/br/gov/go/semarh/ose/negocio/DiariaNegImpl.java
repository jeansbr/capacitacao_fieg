package br.gov.go.semarh.ose.negocio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.EnumStatusDiaria;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.persistencia.DiariaDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("diariaNeg")
public class DiariaNegImpl extends NegocioGenerico<Diaria> implements DiariaNeg{

   private static final long serialVersionUID = 1L;
   
   @Autowired
   public DiariaNegImpl(DiariaDao diariaDao){
      super(diariaDao);
   }

   @Override
   public Diaria consultarPorId(Integer id) throws OseException {
         return ((DiariaDao)persistencia).consultarPorId(id);
   }

   @Override
   public List<Diaria> listar(Date dataInicial, Date dataFinal, Funcionario requisitante, EnumStatusDiaria statusDiaria, Departamento departamento) throws OseException {
      return ((DiariaDao)persistencia).listar(dataInicial, dataFinal, requisitante, statusDiaria, departamento);
   }

}
