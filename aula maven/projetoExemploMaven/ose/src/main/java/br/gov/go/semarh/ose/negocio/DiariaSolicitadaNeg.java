package br.gov.go.semarh.ose.negocio;

import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.DiariaSolicitada;

public interface DiariaSolicitadaNeg extends Negocio<DiariaSolicitada>{
   
}
