package br.gov.go.semarh.ose.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.DiariaSolicitada;
import br.gov.go.semarh.ose.persistencia.DiariaSolicitadaDao;

@Service("diariaSolicitadaNeg")
public class DiariaSolicitadaNegImpl extends NegocioGenerico<DiariaSolicitada> implements DiariaSolicitadaNeg{

   private static final long serialVersionUID = 1L;
   
   @Autowired
   public DiariaSolicitadaNegImpl(DiariaSolicitadaDao diariaSolicitadaDao) {
      super(diariaSolicitadaDao);
   }

}
