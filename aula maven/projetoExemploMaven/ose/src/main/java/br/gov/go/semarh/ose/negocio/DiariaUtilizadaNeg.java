package br.gov.go.semarh.ose.negocio;

import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.util.OseException;

public interface DiariaUtilizadaNeg extends Negocio<DiariaUtilizada>{
	
	public DiariaUtilizada consultar( DiariaUtilizada diariaUtilizada) throws OseException;

}
