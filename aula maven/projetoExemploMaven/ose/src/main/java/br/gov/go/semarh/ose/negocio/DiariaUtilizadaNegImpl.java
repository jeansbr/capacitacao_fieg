package br.gov.go.semarh.ose.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.persistencia.DiariaUtilizadaDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("diariaUtilizadaNeg")
public class DiariaUtilizadaNegImpl extends NegocioGenerico<DiariaUtilizada> implements DiariaUtilizadaNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	public DiariaUtilizadaNegImpl(DiariaUtilizadaDao diariaUtilizadaDao) {
		super(diariaUtilizadaDao);
	}

	@Override
	public DiariaUtilizada consultar(DiariaUtilizada diariaUtilizada)  throws OseException {
		return ((DiariaUtilizadaDao)persistencia).consultar(diariaUtilizada);
	}

}
