package br.gov.go.semarh.ose.negocio;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.Funcionario;

public interface FuncionarioNeg extends Negocio<Funcionario>{

   public List<Funcionario> listar( Integer id, String nome, String matricula, String cargo, String cpf, String gerencia) throws PersistenciaException;
   
}
