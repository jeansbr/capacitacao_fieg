package br.gov.go.semarh.ose.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.persistencia.FuncionarioDao;

@Service("funcionarioNeg")
public class FuncionarioNegImpl extends NegocioGenerico<Funcionario> implements FuncionarioNeg{

   private static final long serialVersionUID = 1L;
   
   @Autowired
   public FuncionarioNegImpl(FuncionarioDao funcionarioDao){
      super(funcionarioDao);
   }

   public List<Funcionario> listar( Integer id, String nome, String matricula, String cargo, String cpf, String gerencia) throws PersistenciaException{
      return ((FuncionarioDao)persistencia).listar( id, nome, matricula, cargo, cpf, gerencia);
   }

}
