package br.gov.go.semarh.ose.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.HistoricoHodometro;
import br.gov.go.semarh.ose.persistencia.HistoricoHodometroDao;

@Service("historicoHodometroNeg")
public class HistoricoHodometroNegImpl extends NegocioGenerico<HistoricoHodometro> implements HistoricoHodometroNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	public HistoricoHodometroNegImpl(HistoricoHodometroDao alteracaoOdometroDao){
		super(alteracaoOdometroDao);
	}
	
}
