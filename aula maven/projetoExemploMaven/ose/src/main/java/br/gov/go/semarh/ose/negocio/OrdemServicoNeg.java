package br.gov.go.semarh.ose.negocio;

import java.util.Date;
import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemServico;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemServico;
import br.gov.go.semarh.ose.util.OseException;

public interface OrdemServicoNeg extends Negocio<OrdemServico>{

   public List<OrdemServico> listar( Integer id, Funcionario requisitante, Date dataHoraInicio, Date dataHoraFim, Date data, EnumStatusOrdemServico estadoOsSelecionado, Departamento departamento) throws OseException;

   public void confirmarOrdemServico(OrdemServico os) throws OseException;

   @Override
	public OrdemServico alterar(OrdemServico entidade)
			throws PersistenciaException;
   
}
