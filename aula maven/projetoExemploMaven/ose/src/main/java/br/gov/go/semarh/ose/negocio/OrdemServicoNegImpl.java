package br.gov.go.semarh.ose.negocio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemServico;
import br.gov.go.semarh.ose.entidade.EnumTipoTransporte;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemServico;
import br.gov.go.semarh.ose.persistencia.OrdemServicoDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("ordemServicoNeg")
public class OrdemServicoNegImpl extends NegocioGenerico<OrdemServico> implements OrdemServicoNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	private DiariaNeg diariaNeg;
	
	@Autowired
	public OrdemServicoNegImpl(OrdemServicoDao ordemServicoDao){
		super(ordemServicoDao);
	}

	public List<OrdemServico> listar( Integer id, Funcionario requisitante, Date dataHoraInicio, Date dataHoraFim, Date data, EnumStatusOrdemServico estadoOsSelecionado, Departamento departamento) throws OseException{
		return ((OrdemServicoDao)persistencia).listar( id, requisitante, dataHoraInicio, dataHoraFim, data, estadoOsSelecionado, departamento);
	}

	@Override
	@Transactional(rollbackFor=OseException.class)
	public void confirmarOrdemServico(OrdemServico ordemServico) throws OseException {
		try {
			ordemServico.setData(new Date());
			ordemServico.confirmarOrdemServico();
			if(ordemServico.isNecessitaDiaria()){
				ordemServico.gerarDiaria();
				for(Diaria diaria : ordemServico.getDiarias()){
					diariaNeg.incluir(diaria);
				}
			}

			if(ordemServico.getTipoTransporte() == EnumTipoTransporte.VEICULO_OFICIAL){
				ordemServico.gerarOrdemTrafego();
			}

			alterar(ordemServico);
		} catch (PersistenciaException e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new OseException(e.getMessage(), e);
		} catch (RuntimeException e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new OseException(e.getMessage(), e);
		}
	}
}
