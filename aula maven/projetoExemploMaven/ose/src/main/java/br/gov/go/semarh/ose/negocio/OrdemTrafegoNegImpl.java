package br.gov.go.semarh.ose.negocio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemTrafego;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemTrafego;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.persistencia.OrdemTrafegoDao;

@Service("ordemTrafegoNeg")
public class OrdemTrafegoNegImpl extends NegocioGenerico<OrdemTrafego> implements OrdemTrafegoNeg{

   private static final long serialVersionUID = 1L;
   
   @Autowired
   public OrdemTrafegoNegImpl(OrdemTrafegoDao ordemTrafegoDao){
      super(ordemTrafegoDao);
   }

   @Override
   public List<OrdemTrafego> listar(Integer id, Funcionario requisitante, Date dataInicial, Date dataFinal, EnumStatusOrdemTrafego statusSelecionadoOT, Veiculo veiculo, Departamento departamento) throws PersistenciaException {
      return ((OrdemTrafegoDao)persistencia).listar( id, requisitante, dataInicial, dataFinal, statusSelecionadoOT, veiculo, departamento);
   }
   
   public String numeroDaAutorizacao() throws PersistenciaException{
	   return ((OrdemTrafegoDao)persistencia).numeroDaAutorizacao();
   }

}
