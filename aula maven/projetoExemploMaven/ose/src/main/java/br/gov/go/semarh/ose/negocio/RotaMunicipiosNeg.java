package br.gov.go.semarh.ose.negocio;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.util.OseException;

public interface RotaMunicipiosNeg extends Negocio<RotaMunicipios>{

	public List<RotaMunicipios> listar(Integer id, String descricao) throws PersistenciaException;

	public Object consultarPorDescricao(String descricao) throws OseException;
}
