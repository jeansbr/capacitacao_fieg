package br.gov.go.semarh.ose.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.persistencia.RotaMunicipiosDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("rotaMunicipiosNeg")
public class RotaMunicipiosNegImpl extends NegocioGenerico<RotaMunicipios> implements RotaMunicipiosNeg {

	private static final long serialVersionUID = 1L;

	@Autowired
	public RotaMunicipiosNegImpl(RotaMunicipiosDao rotaMunicipiosDao) {
		super(rotaMunicipiosDao);
		
	}

	public List<RotaMunicipios> listar(Integer id, String descricao) throws PersistenciaException {
		return ((RotaMunicipiosDao) persistencia).listar(id, descricao);
	}

	public Object consultarPorDescricao(String descricao) throws OseException {		
		return ((RotaMunicipiosDao) persistencia).consultarPorDescricao(descricao);
	}

}
