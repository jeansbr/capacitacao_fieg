package br.gov.go.semarh.ose.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.ServicoExecutado;
import br.gov.go.semarh.ose.persistencia.ServicoExecutadoDao;

@Service("servicoExecutadoNeg")
public class ServicoExecutadoNegImpl extends NegocioGenerico<ServicoExecutado> implements ServicoExecutadoNeg{

   private static final long serialVersionUID = 1L;
   
   @Autowired
   public ServicoExecutadoNegImpl(ServicoExecutadoDao servicoExecutadoDao){
      super(servicoExecutadoDao);
   }

}
