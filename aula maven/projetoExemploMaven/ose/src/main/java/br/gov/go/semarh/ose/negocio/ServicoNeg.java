package br.gov.go.semarh.ose.negocio;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.util.OseException;

public interface ServicoNeg extends Negocio<Servico>{


	public List<Servico> listar( Integer id, String descricao) throws PersistenciaException;
	
	public Servico consultarPorDescricao(String descricao) throws OseException;

}
