package br.gov.go.semarh.ose.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.persistencia.ServicoDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("servicoNeg")
public class ServicoNegImpl extends NegocioGenerico<Servico> implements ServicoNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	public ServicoNegImpl(ServicoDao servicoDao){
		super(servicoDao);
	}

	public List<Servico> listar( Integer id, String descricao) throws PersistenciaException{
		return ((ServicoDao)persistencia).listar( id, descricao);
	}
	
	public Servico consultarPorDescricao( String descricao) throws OseException{
		return ((ServicoDao)persistencia).consultarPorDescricao(descricao);
	}
}
