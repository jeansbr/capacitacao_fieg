package br.gov.go.semarh.ose.negocio;

import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.util.OseException;

public interface ValorDiariaNeg extends Negocio<ValorDiaria>{

	public ValorDiaria consultarAtivo() throws OseException;

}
