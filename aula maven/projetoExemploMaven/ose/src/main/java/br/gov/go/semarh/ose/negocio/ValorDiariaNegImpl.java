package br.gov.go.semarh.ose.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.persistencia.ValorDiariaDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("valorDiariaNeg")
public class ValorDiariaNegImpl extends NegocioGenerico<ValorDiaria> implements ValorDiariaNeg{

	private static final long serialVersionUID = 1L;

	@Autowired
	public ValorDiariaNegImpl(ValorDiariaDao valorDiariaDao) {
		super(valorDiariaDao);
		
	}

	@Override
	public ValorDiaria consultarAtivo() throws OseException {
		return ((ValorDiariaDao)persistencia).consultarAtivo();
	}

}
