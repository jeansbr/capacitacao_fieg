package br.gov.go.semarh.ose.negocio;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.Negocio;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.util.OseException;

public interface VeiculoNeg extends Negocio<Veiculo>{
   
   public List<Veiculo> listar( Integer id, String placa, String marcaModelo, String chassi, String tipoVeiculo, Integer hodometro) throws PersistenciaException;
   
   public Veiculo consultarPorPlaca(String placa) throws OseException;



}
