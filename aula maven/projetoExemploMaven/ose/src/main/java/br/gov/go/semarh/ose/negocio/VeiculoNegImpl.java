package br.gov.go.semarh.ose.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.negocio.NegocioGenerico;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.persistencia.VeiculoDao;
import br.gov.go.semarh.ose.util.OseException;

@Service("veiculoNeg")
public class VeiculoNegImpl extends NegocioGenerico<Veiculo> implements VeiculoNeg {

	private static final long serialVersionUID = 1L;

	@Autowired
	public VeiculoNegImpl(VeiculoDao veiculoDao) {
		super(veiculoDao);

	}
	public List<Veiculo> listar( Integer id, String placa, String marcaModelo, String chassi, String tipoVeiculo, Integer hodometro) throws PersistenciaException{
		return ((VeiculoDao)persistencia).listar( id, placa, marcaModelo, chassi, tipoVeiculo, hodometro);
	}
	
	@Override
	public Veiculo consultarPorPlaca(String placa) throws OseException {
		
		return ((VeiculoDao)persistencia).consultarPorPlaca(placa);
	}


}
