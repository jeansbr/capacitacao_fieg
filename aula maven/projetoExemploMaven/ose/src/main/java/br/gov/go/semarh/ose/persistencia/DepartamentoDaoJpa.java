package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.util.OseException;

@Repository("departamentoDao")
public class DepartamentoDaoJpa extends PersistenciaJpa<Departamento> implements DepartamentoDao{

	private static final long serialVersionUID = 1L;

	@Override
	public Departamento consultarPorNome(String nome) throws OseException {
		String sql = "select d from Departamento d where upper(d.nome) = upper(:nome)";
		Query query = em.createQuery(sql);
		query.setParameter("nome", nome);
		try {
			return (Departamento) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Departamento> listar(Integer id, String nome) throws PersistenciaException {
		
		try {
			String sql = "select d from Departamento d where 1 = 1";
			if (id != null && id>0) {
				sql += " and d.id = :id";
			}
			if (nome != null && !nome.isEmpty()) {
				sql += " and upper(d.nome) like upper(:nome)";
			}
			Query query = em.createQuery(sql);
			if (id != null && id>0) {
				query.setParameter("id", id);
			}
			if (nome != null && !nome.isEmpty()) {
				query.setParameter("nome", "%" + nome + "%");
			}
			return query.getResultList();
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}

}
