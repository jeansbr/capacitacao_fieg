package br.gov.go.semarh.ose.persistencia;

import java.util.Date;
import java.util.List;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.EnumStatusDiaria;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.util.OseException;

public interface DiariaDao extends Persistencia<Diaria>{

   public Diaria consultarPorId(Integer id) throws OseException;

   public List<Diaria> listar(Date dataInicial, Date dataFinal, Funcionario requisitante, EnumStatusDiaria statusDiaria, Departamento departamento) throws OseException;

   
}
