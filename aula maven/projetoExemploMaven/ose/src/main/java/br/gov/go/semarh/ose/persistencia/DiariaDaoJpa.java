package br.gov.go.semarh.ose.persistencia;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.Diaria;
import br.gov.go.semarh.ose.entidade.EnumStatusDiaria;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.util.OseException;

@Repository("diariaDao")
public class DiariaDaoJpa extends PersistenciaJpa<Diaria> implements DiariaDao{

   private static final long serialVersionUID = 1L;

   @Override
   public Diaria consultarPorId(Integer id) throws OseException {
      try {
         String sql = "select distinct d from Diaria d "
               + " where d.id = :id";
         
         Query query = em.createQuery(sql);
         if (id != null) {
            query.setParameter("id", id);
         }
         return (Diaria) query.getSingleResult();
      } catch (NoResultException nre) {
         return null;
      } catch (Exception e) {
         UtilLog.getLog().error(e.getMessage(), e);
         throw new OseException(e.getMessage());
      }
   }

   @SuppressWarnings("unchecked")
   @Override
   public List<Diaria> listar(Date dataInicio, Date dataFim, Funcionario funcionarios, EnumStatusDiaria statusDiaria, Departamento departamento) throws OseException {
      try {
         String sql = "select distinct d from Diaria d " +
        		 "left join fetch d.servidor f " +
                " where 1 = 1";
         if (dataInicio != null && dataFim != null) {
            sql+= " and d.dataRegistro >= :dataInicio and d.dataRegistro <= :dataFim ";
         }
         if (funcionarios != null) {
            sql+= " and f = :funcionarios";
         }         
         if (statusDiaria != null) {
        	 sql += " and d.status = :statusDiaria";
         }
         if (departamento != null){
        	 sql += " and d.departamento = :departamento";
         }
         
         Query query = em.createQuery(sql);
         
         
         if (dataInicio != null) {
            query.setParameter("dataInicio", dataInicio);
         }
         if (dataFim != null) {
            query.setParameter("dataFim", dataFim);
         }
         if (funcionarios != null) {
            query.setParameter("funcionarios", funcionarios);
         }
         if( statusDiaria != null){
        	 query.setParameter("statusDiaria", statusDiaria);
         }
         if( departamento !=null){
        	 query.setParameter("departamento", departamento);
         }
         return query.getResultList();
      } catch (NoResultException nre) {
         return null;
      } catch (Exception e) {
         UtilLog.getLog().error(e.getMessage(), e);
         throw new OseException(e.getMessage());
      }
   }
}
