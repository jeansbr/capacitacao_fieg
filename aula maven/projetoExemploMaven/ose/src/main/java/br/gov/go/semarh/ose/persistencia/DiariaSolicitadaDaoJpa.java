package br.gov.go.semarh.ose.persistencia;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.gov.go.semarh.ose.entidade.DiariaSolicitada;

@Repository("diariaSolicitadaDao")
public class DiariaSolicitadaDaoJpa extends PersistenciaJpa<DiariaSolicitada> implements DiariaSolicitadaDao{

   private static final long serialVersionUID = 1L;

}
