package br.gov.go.semarh.ose.persistencia;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.util.OseException;

public interface DiariaUtilizadaDao extends Persistencia<DiariaUtilizada> {

	public DiariaUtilizada consultar(DiariaUtilizada diariaUtilizada) throws OseException;
}
