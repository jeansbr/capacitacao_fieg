package br.gov.go.semarh.ose.persistencia;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.DiariaUtilizada;
import br.gov.go.semarh.ose.util.OseException;

@Repository("diariaUtilizadaDao")
public class DiariaUtilizadaDaoJpa extends PersistenciaJpa<DiariaUtilizada> implements DiariaUtilizadaDao {

	private static final long serialVersionUID = 1L;
	
	@Override
	public DiariaUtilizada consultar(DiariaUtilizada diariaUtilizada) throws OseException {
		try {
			Query query = em.createQuery("select d from DiariaUtilizada d where d = :diariaUtilizada");
			query.setParameter("diariaUtilizada", diariaUtilizada);
			diariaUtilizada = (DiariaUtilizada) query.getSingleResult();
			diariaUtilizada.getDocumentos().size();
			return diariaUtilizada;
		} catch (NoResultException e) {
			return null;
		} catch (RuntimeException e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new OseException(e.getMessage(), e);
		}
	}

}
