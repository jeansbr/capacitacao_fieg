package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.Funcionario;


public interface FuncionarioDao extends Persistencia<Funcionario>{

   public List<Funcionario> listar( Integer id, String nome, String matricula, String cargo, String cpf, String gerencia) throws PersistenciaException;
   
   public Funcionario consultarPorUsuario(String email) throws PersistenciaException;

}
