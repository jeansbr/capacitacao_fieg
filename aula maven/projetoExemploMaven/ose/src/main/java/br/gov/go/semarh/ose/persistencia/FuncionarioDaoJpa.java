package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Funcionario;


@Repository("funcionarioDao")
public class FuncionarioDaoJpa extends PersistenciaJpa<Funcionario> implements FuncionarioDao{

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<Funcionario> listar( Integer id, String nome, String matricula, String cargo, String cpf, String gerencia) throws PersistenciaException{
		try{
			String sql = "select distinct e from Funcionario e left join e.contratos c left join e.departamentos d where c.dataTermino = null and d.dataSaida = null ";
			if(id != null){
				sql += " and e.id = :id";
			}
			if(nome != null && !nome.isEmpty()){
				sql += " and upper(e.nome) like upper(:nome)";
			}
			if(matricula != null){
				sql += " and upper(e.matricula) like upper(:matricula)";
			}
			if(cargo != null){
				sql += " and upper(e.cargo) like upper(:cargo)";
			}
			if(cpf != null){
				sql += " and upper(e.cpf) like upper(:cpf)";
			}
			if(gerencia != null){
				sql += " and upper(e.gerencia) like upper(:gerencia)";
			}
			Query query = em.createQuery(sql);
			if(id != null){
				query.setParameter("id", id);
			}
			if(nome != null && !nome.isEmpty()){
				query.setParameter("nome", "%" + nome + "%");
			}
			if(matricula != null){
				query.setParameter("matricula", "%" + matricula + "%");
			}
			if(cargo != null){
				query.setParameter("cargo", "%" + cargo + "%");
			}
			if(cpf != null){
				query.setParameter("cpf", "%" + cpf + "%");
			}
			if(gerencia != null){
				query.setParameter("gerencia", "%" + gerencia + "%");
			}
			return query.getResultList();
		}catch(Exception e){
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public Funcionario consultarPorUsuario(String email)
			throws PersistenciaException {
		try {
			Query query = em.createQuery("select f from Funcionario f join f.usuario u where u.email = :email");
			query.setParameter("email", email);
			return (Funcionario) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}
	
	@Override
	public Funcionario consultar(Object codigo) throws PersistenciaException {
		try {
			Funcionario func = super.consultar(codigo);
			func.getContratos().size();
			func.getDepartamentos().size();
			
			return func;
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage(), e);
		}
	}
}
