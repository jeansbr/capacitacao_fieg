package br.gov.go.semarh.ose.persistencia;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.HistoricoHodometro;

public interface HistoricoHodometroDao extends Persistencia<HistoricoHodometro>{

}
