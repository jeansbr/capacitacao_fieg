package br.gov.go.semarh.ose.persistencia;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.gov.go.semarh.ose.entidade.HistoricoHodometro;

@Repository("HistoricoHodometroDao")
public class HistoricoHodometroDaoJpa extends PersistenciaJpa<HistoricoHodometro> implements HistoricoHodometroDao{

   private static final long serialVersionUID = 1L;

}
