package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Municipio;

public interface MunicipioDao extends Persistencia<Municipio>{

   List<Municipio> listarPorUf(EnumUf uf) throws PersistenciaException;
   
}
