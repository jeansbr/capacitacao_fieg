package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.EnumUf;
import br.gov.go.semarh.ose.entidade.Municipio;

@Repository("municipioDao")
public class MunicipioDaoJpa extends PersistenciaJpa<Municipio> implements MunicipioDao{

   private static final long serialVersionUID = 1L;

   @SuppressWarnings("unchecked")
   @Override
   public List<Municipio> listarPorUf(EnumUf uf) throws PersistenceException {
      try {
         Query query = em.createQuery("select m from Municipio m where m.uf = :uf order by m.descricao");
         query.setParameter("uf", uf);
         return query.getResultList();
      } catch (Exception e) {
         UtilLog.getLog().error(e.getMessage(), e);
         throw new PersistenceException(e.getMessage());
      }
   }

}
