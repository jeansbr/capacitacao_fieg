package br.gov.go.semarh.ose.persistencia;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemServico;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemServico;
import br.gov.go.semarh.ose.util.OseException;

@Repository("ordemServicoDao")
public class OrdemServicoDaoJpa extends PersistenciaJpa<OrdemServico> implements OrdemServicoDao{

	private static final long serialVersionUID = 1L;

	@Override
	public OrdemServico consultar(Object codigo) throws PersistenciaException {
		try {
			OrdemServico os = super.consultar(codigo);
			os.getDiarias().size();
			os.getFuncionarios().size();
			os.getMunicipios().size();
			os.getServicosExecutados().size();
			return os;
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<OrdemServico> listar( Integer id, Funcionario requisitante, Date dataHoraInicio, Date dataHoraFim, Date data, EnumStatusOrdemServico estadoOsSelecionado, Departamento departamento) throws OseException{
		try{
			String sql = "select distinct e from OrdemServico e " +
					"left join fetch e.requisitante r " +
					"left join fetch e.funcionarios f " +
					"left join fetch e.servicosExecutados se " +
					"left join fetch e.ordemTrafego ot " +
					"where 1 = 1";
			if(id != null){
				sql += " and e.id =:id";
			}
			if(dataHoraInicio != null && dataHoraFim != null){
				sql += " and e.dataHoraInicio >= :dataHoraInicio and e.dataHoraInicio <= :dataHoraFim";
			}
			if(data != null){
				sql += " and e.data = :data";
			}
			if(requisitante != null && requisitante.getId() != null && requisitante.getId() > 0){
				sql += " and e.requisitante = :requisitante";
			}
			if(estadoOsSelecionado != null){
				sql += " and e.estadoOrdemServico = :estadoOsSelecionado";
			}else{
				sql += " and e.estadoOrdemServico != :estadoOsSelecionado";
			}
			if(departamento != null){
				sql+= " and e.departamento =:departamento";
			}
			Query query = em.createQuery(sql);
			if(id != null){
				query.setParameter("id", id);
			}
			if(dataHoraInicio != null){
				query.setParameter("dataHoraInicio", dataHoraInicio);
			}
			if(dataHoraFim != null){
				query.setParameter("dataHoraFim", dataHoraFim);
			}
			if(data != null){
				query.setParameter("data", data);
			}
			if(requisitante != null && requisitante.getId() != null && requisitante.getId() > 0){
				query.setParameter("requisitante", requisitante);
			}
			if(estadoOsSelecionado != null){
				query.setParameter("estadoOsSelecionado", estadoOsSelecionado);
			}else{
				query.setParameter("estadoOsSelecionado", EnumStatusOrdemServico.CANCELADA);
			}
			if(departamento != null){
				query.setParameter("departamento", departamento);
			}
			return query.getResultList();
		}catch(Exception e){
			UtilLog.getLog().error(e.getMessage(), e);
			throw new OseException(e.getMessage());
		}
	}
}
