package br.gov.go.semarh.ose.persistencia;

import java.util.Date;
import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemTrafego;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemTrafego;
import br.gov.go.semarh.ose.entidade.Veiculo;

public interface OrdemTrafegoDao extends Persistencia<OrdemTrafego>{

   public List<OrdemTrafego> listar(Integer id, Funcionario requisitante, Date dataInicial, Date dataFinal, EnumStatusOrdemTrafego statusSelecionadoOT, Veiculo veiculo, Departamento departamento) throws PersistenciaException;
   
   public String numeroDaAutorizacao() throws PersistenciaException;
}
