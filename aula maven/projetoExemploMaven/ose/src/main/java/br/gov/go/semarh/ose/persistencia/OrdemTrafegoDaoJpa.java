package br.gov.go.semarh.ose.persistencia;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.entidade.EnumStatusOrdemTrafego;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.entidade.OrdemTrafego;
import br.gov.go.semarh.ose.entidade.Veiculo;

@Repository("ordemTrafegoDao")
public class OrdemTrafegoDaoJpa extends PersistenciaJpa<OrdemTrafego> implements OrdemTrafegoDao{

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemTrafego> listar(Integer id, Funcionario requisitante, Date dataInicial, Date dataFinal, EnumStatusOrdemTrafego statusSelecionadoOT, Veiculo veiculo, Departamento departamento) throws PersistenciaException {
		try{
			String sql = "select distinct e from OrdemTrafego e " +
					"left join fetch e.veiculo v " +
					"where 1 = 1";
			if(id != null && id.intValue()!= 0){
				sql += " and e.id =:id ";
			}else {

				if(dataInicial != null && dataFinal != null){
					sql += " and e.dataHoraSaida >= :dataHoraSaida and e.dataHoraSaida <= :dataHoraRetorno";
				}

				if(requisitante != null && requisitante.getId() != null && requisitante.getId() > 0){
					sql += " and e.requisitante = :requisitante";
				}

				if(statusSelecionadoOT !=null){
					sql += " and e.status = :statusSelecionadoOT";
				}
				
				if(veiculo != null && veiculo.getPlaca() !=null){
					sql += " and e.veiculo = :veiculo";
				}
				if(departamento != null){
					sql += " and e.departamento = :departamento";
				}
			}

			Query query = em.createQuery(sql);

			if(id != null && id.intValue()!= 0){
				query.setParameter("id", id);
			}else {
				if(dataInicial != null){
					query.setParameter("dataHoraSaida", dataInicial);
				}
				if(dataFinal != null){
					query.setParameter("dataHoraRetorno", dataFinal);
				}

				if(requisitante != null && requisitante.getId() != null && requisitante.getId() > 0){
					query.setParameter("requisitante", requisitante);
				}

				if(statusSelecionadoOT !=null){
					query.setParameter("statusSelecionadoOT", statusSelecionadoOT);
				}
				
				if(veiculo != null && veiculo.getPlaca()!=null){
					query.setParameter("veiculo", veiculo);
				}
				
				if(departamento != null ){
					query.setParameter("departamento", departamento);
				}
			}

			return query.getResultList();
		}catch(Exception e){
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public OrdemTrafego consultar(Object codigo) throws PersistenciaException {
		String sql = "select e from OrdemTrafego e " +				
				" where e.id = :codigo";
		Query query = em.createQuery(sql);
		query.setParameter("codigo", codigo);
		return (OrdemTrafego) query.getSingleResult();
	}
	
	public String numeroDaAutorizacao() throws PersistenciaException {
		Integer numero; 
		try{			
			String sql = "select e.codigoAutorizacao from OrdemTrafego e " +
					"where e.dataAutorizacao = (select max(ot.dataAutorizacao) from OrdemTrafego ot where date_part('YEAR', ot.dataregistro) =  DATE_PART('YEAR', CURRENT_TIMESTAMP))";
			Query query = em.createQuery(sql);
			String[] codigoAnterior = ((String) query.getSingleResult()).split("/");
			numero = Integer.valueOf(codigoAnterior[0]) + 1;
		}catch(NoResultException e){
			numero = Integer.valueOf(1);
		}
		Calendar caledario = Calendar.getInstance();
		return String.valueOf(numero) + "/" + String.valueOf(caledario.get(Calendar.YEAR));
	}
}
