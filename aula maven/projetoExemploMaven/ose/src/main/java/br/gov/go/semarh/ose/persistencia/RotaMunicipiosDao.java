package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;

public interface RotaMunicipiosDao extends Persistencia<RotaMunicipios>{

	public List<RotaMunicipios> listar(Integer id, String descricao) throws PersistenciaException;

	public RotaMunicipios consultarPorDescricao(String descricao);
	

}
