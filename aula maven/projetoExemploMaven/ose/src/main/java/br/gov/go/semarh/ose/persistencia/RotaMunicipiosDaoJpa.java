package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;

@Repository("rotaMunicipiosDao")
public class RotaMunicipiosDaoJpa extends PersistenciaJpa<RotaMunicipios> implements RotaMunicipiosDao{

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<RotaMunicipios> listar(Integer id, String descricao) throws PersistenciaException {
		try {
			String sql = "select r from RotaMunicipios r where 1 = 1";
			if (id != null && id>0) {
				sql += " and e.id = :id";
			}
			if (descricao != null && !descricao.isEmpty()) {
				sql += " and upper(r.descricao) like upper(:descricao)";
			}
			Query query = em.createQuery(sql);
			if (id != null && id>0) {
				query.setParameter("id", id);
			}
			if (descricao != null && !descricao.isEmpty()) {
				query.setParameter("descricao", "%" + descricao + "%");
			}
			return query.getResultList();
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}
	
	@Override
	public RotaMunicipios consultar(Object codigo) throws PersistenciaException {
		try {
			RotaMunicipios rm = super.consultar(codigo);			
			rm.getMunicipios().size();
			return rm;
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage(), e);
		}
	}


	@Override
	public RotaMunicipios consultarPorDescricao(String descricao) {
		// TODO Auto-generated method stub
		return null;
	}

}
