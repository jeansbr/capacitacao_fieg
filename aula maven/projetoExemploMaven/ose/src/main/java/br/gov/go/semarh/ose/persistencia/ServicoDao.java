package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.util.OseException;

public interface ServicoDao extends Persistencia<Servico>{

   public List<Servico> listar( Integer id, String descricao) throws PersistenciaException;
   public Servico consultarPorDescricao(String descricao) throws OseException;
}
