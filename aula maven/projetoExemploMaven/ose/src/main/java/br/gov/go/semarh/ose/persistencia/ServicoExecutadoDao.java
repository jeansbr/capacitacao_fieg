package br.gov.go.semarh.ose.persistencia;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.ServicoExecutado;

public interface ServicoExecutadoDao extends Persistencia<ServicoExecutado>{

}
