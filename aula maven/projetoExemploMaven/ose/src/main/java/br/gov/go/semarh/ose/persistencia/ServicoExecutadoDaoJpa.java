package br.gov.go.semarh.ose.persistencia;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.gov.go.semarh.ose.entidade.ServicoExecutado;

@Repository("servicoExecutadoDao")
public class ServicoExecutadoDaoJpa extends PersistenciaJpa<ServicoExecutado> implements ServicoExecutadoDao{

   private static final long serialVersionUID = 1L;

}
