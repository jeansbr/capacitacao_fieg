package br.gov.go.semarh.ose.persistencia;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.util.OseException;

public interface ValorDiariaDao extends Persistencia<ValorDiaria>{

	public ValorDiaria consultarAtivo() throws OseException;

}
