package br.gov.go.semarh.ose.persistencia;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.gov.go.semarh.ose.entidade.ValorDiaria;
import br.gov.go.semarh.ose.util.OseException;

@Repository("valorDiariaDao")
public class ValorDiariaDaoJpa extends PersistenciaJpa<ValorDiaria> implements ValorDiariaDao{

	private static final long serialVersionUID = 1L;

	@Override
	public ValorDiaria consultarAtivo() throws OseException {
		String sql = "select v from ValorDiaria v where v.ativo = :valor)";
		Query query = em.createQuery(sql);
		query.setParameter("valor", true);
		try {
			return (ValorDiaria) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
