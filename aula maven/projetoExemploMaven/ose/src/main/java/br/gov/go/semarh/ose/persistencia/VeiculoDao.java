package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.util.OseException;

public interface VeiculoDao extends Persistencia<Veiculo>{
   
	public List<Veiculo> listar( Integer id, String placa, String marcaModelo, String chassi, String tipoVeiculo, Integer hodometro) throws PersistenciaException;

	Veiculo consultarPorPlaca(String placa) throws OseException;

   
}
