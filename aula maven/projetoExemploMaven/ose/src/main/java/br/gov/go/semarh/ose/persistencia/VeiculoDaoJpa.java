package br.gov.go.semarh.ose.persistencia;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.persistencia.PersistenciaJpa;
import br.com.ambientinformatica.util.UtilLog;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.util.OseException;

@Repository("veiculoDao")
public class VeiculoDaoJpa extends PersistenciaJpa<Veiculo> implements VeiculoDao {

	private static final long serialVersionUID = 1L;

	@Override
	public Veiculo consultarPorPlaca(String placa) throws OseException {
		try {
			String sql = "select distinct v from Veiculo v "
					+ " where v.placa = :placa";

			Query query = em.createQuery(sql);
			if (placa != null) {
				query.setParameter("placa", placa);
			}
			return (Veiculo) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			UtilLog.getLog().error(e.getMessage(), e);
			throw new OseException(e.getMessage());
		}
	}


	@SuppressWarnings("unchecked")
	public List<Veiculo> listar( Integer id, String placa, String marcaModelo, String chassi, String tipoVeiculo, Integer hodometro) throws PersistenciaException{
		try{
			String sql = "select e from Veiculo e left join fetch e.historicoOdometro h where 1 = 1";
			if(id != null){
				sql += " and e.id = :id";
			}
			if(placa != null){
				sql += " and upper(e.placa) like upper(:placa)";
			}
			if(marcaModelo != null){
				sql += " and upper(e.marcaModelo) like upper(:marcaModelo)";
			}
			if(chassi != null){
				sql += " and upper(e.chassi) like upper(:chassi)";
			}
			if(tipoVeiculo != null){
				sql += " and upper(e.tipoVeiculo) like upper(:tipoVeiculo)";
			}
			if(hodometro != null){
				sql += " and upper(e.hodometro) like upper(:hodometro)";
			}
			Query query = em.createQuery(sql);
			if(id != null){
				query.setParameter("id", id);
			}
			if(placa != null){
				query.setParameter("placa", "%" + placa + "%");
			}
			if(marcaModelo != null){
				query.setParameter("marcaModelo", "%" + marcaModelo + "%");
			}
			if(chassi != null){
				query.setParameter("chassi", "%" + chassi + "%");
			}
			if(tipoVeiculo != null){
				query.setParameter("tipoVeiculo", "%" + tipoVeiculo + "%");
			}
			if(hodometro != null){
				query.setParameter("hodometro", "%" + hodometro + "%");
			}
			return query.getResultList();
		}catch(Exception e){
			UtilLog.getLog().error(e.getMessage(), e);
			throw new PersistenciaException(e.getMessage());
		}
	}

}
