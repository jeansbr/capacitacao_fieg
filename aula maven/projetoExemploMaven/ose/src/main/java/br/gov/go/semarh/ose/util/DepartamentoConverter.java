package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.Departamento;
import br.gov.go.semarh.ose.negocio.DepartamentoNeg;

@FacesConverter("departamentoConverter")
public class DepartamentoConverter implements Converter {  

   private DepartamentoNeg departamentoNeg = (DepartamentoNeg)FabricaAbstrata.criarObjeto("departamentoNeg");
   
   @Override
   public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
       if (value == null || value.equals("")) {  
           return "";  
       } else {  
           return String.valueOf(((Departamento) value).getId());  
       }  
   }


   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      if (value != null && !value.trim().equals("")) {  
         Departamento departamento = new Departamento();
         try {  
            int id = Integer.parseInt(value);  

            try {
               departamento = departamentoNeg.consultar(id);
            } catch (PersistenciaException e) {
               e.printStackTrace();
            }
         } catch(NumberFormatException exception) {  
            return null;
         }  
         return departamento;  
      }else{
         return null;
      }
   }
}  