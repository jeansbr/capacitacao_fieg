package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.Funcionario;
import br.gov.go.semarh.ose.negocio.FuncionarioNeg;

@FacesConverter("funcionarioConverter")
public class FuncionarioConverter implements Converter {  

   private FuncionarioNeg funcionarioNeg = (FuncionarioNeg)FabricaAbstrata.criarObjeto("funcionarioNeg");
   
   @Override
   public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
       if (value == null || value.equals("")) {  
           return "";  
       } else {  
           return String.valueOf(((Funcionario) value).getId());  
       }  
   }


   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      if (value != null && !value.trim().equals("")) {  
         Funcionario funcionario = new Funcionario();
         try {  
            int id = Integer.parseInt(value);  

            try {
               funcionario = funcionarioNeg.consultar(id);
            } catch (PersistenciaException e) {
               e.printStackTrace();
            }
         } catch(NumberFormatException exception) {  
//            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Funcionario escolhido não é válido"));
            return null;
         }  
         return funcionario;  
      }else{
         return null;
      }
   }
}  

