package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.Municipio;
import br.gov.go.semarh.ose.persistencia.MunicipioDao;

@FacesConverter("municipioConverter")
public class MunicipioConverter implements Converter{

   private MunicipioDao municipioDao = (MunicipioDao) FabricaAbstrata.criarObjeto("municipioDao");
   
   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      try {
         Integer id = Integer.parseInt(value);
         return municipioDao.consultar(id);
      } catch (Exception e) {
         return null;
      }
   }

   @Override
   public String getAsString(FacesContext context, UIComponent component, Object value) {
      try {
         return String.valueOf(((Municipio)value).getId());
      } catch (Exception e) {
         return "";
      }
   }

}
