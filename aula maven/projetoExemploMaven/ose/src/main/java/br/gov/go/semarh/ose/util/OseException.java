package br.gov.go.semarh.ose.util;

import java.util.List;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;

public class OseException extends PersistenciaException{

   private static final long serialVersionUID = 1L;

   public OseException() {
      super();
   }

   public OseException(List<String> listaMensagens) {
      super(listaMensagens);
   }

   public OseException(String message, Throwable cause) {
      super(message, cause);
   }

   public OseException(String message) {
      super(message);
   }

   public OseException(Throwable cause) {
      super(cause);
   }
}