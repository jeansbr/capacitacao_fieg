package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.RotaMunicipios;
import br.gov.go.semarh.ose.negocio.RotaMunicipiosNeg;

@FacesConverter("rotaMunicipiosConverter")
public class RotaMunicipiosConverter  implements Converter{

	private RotaMunicipiosNeg rotaMunicipiosNeg = (RotaMunicipiosNeg)FabricaAbstrata.criarObjeto("rotaMunicipiosNeg");

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.trim().equals("")) {  
			RotaMunicipios rotaMunicipios = new RotaMunicipios();
			try {  
				int id = Integer.parseInt(value);  

				try {
					rotaMunicipios = rotaMunicipiosNeg.consultar(id);
				} catch (PersistenciaException e) {
					e.printStackTrace();
				}
			} catch(NumberFormatException exception) {  
				return null;
			}  
			return rotaMunicipios;  
		}else{
			return null;
		}
	}


	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null || value.equals("")) {  
			return "";  
		} else {  
			return String.valueOf(((RotaMunicipios) value).getId());  
		}  
	}

}
