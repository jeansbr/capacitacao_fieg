package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.Servico;
import br.gov.go.semarh.ose.negocio.ServicoNeg;

@FacesConverter("servicoConverter")
public class ServicoConverter implements Converter {  

   private ServicoNeg servicoNeg = (ServicoNeg)FabricaAbstrata.criarObjeto("servicoNeg");
   
   @Override
   public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
       if (value == null || value.equals("")) {  
           return "";  
       } else {  
           return String.valueOf(((Servico) value).getId());  
       }  
   }


   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      if (value != null && !value.trim().equals("")) {  
         Servico servico = new Servico();
         try {  
            int id = Integer.parseInt(value);  

            try {
               servico = servicoNeg.consultar(id);
            } catch (PersistenciaException e) {
               e.printStackTrace();
            }
         } catch(NumberFormatException exception) {  

            return null;
         }  
         return servico;  
      }else{
         return null;
      }
   }
}  

