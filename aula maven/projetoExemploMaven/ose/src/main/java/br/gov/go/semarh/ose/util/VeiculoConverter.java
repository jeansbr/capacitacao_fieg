package br.gov.go.semarh.ose.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.ambientinformatica.jpa.exception.PersistenciaException;
import br.com.ambientinformatica.jpa.util.FabricaAbstrata;
import br.gov.go.semarh.ose.entidade.Veiculo;
import br.gov.go.semarh.ose.negocio.VeiculoNeg;

@FacesConverter("veiculoConverter")
public class VeiculoConverter implements Converter {  

   private VeiculoNeg veiculoNeg = (VeiculoNeg)FabricaAbstrata.criarObjeto("veiculoNeg");
   
   @Override
   public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
       if (value == null || value.equals("")) {  
           return "";  
       } else {  
           return String.valueOf(((Veiculo) value).getId());  
       }  
   }


   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      if (value != null && !value.trim().equals("")) {  
         Veiculo veiculo = new Veiculo();
         try {  
            int id = Integer.parseInt(value);  

            try {
               veiculo = veiculoNeg.consultar(id);
            } catch (PersistenciaException e) {
               e.printStackTrace();
            }
         } catch(NumberFormatException exception) {  
            return null;
         }  
         return veiculo;  
      }else{
         return null;
      }
   }
}  

