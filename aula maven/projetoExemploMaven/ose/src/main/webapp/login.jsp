<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html; charset=iso-8859-1" %>
<html id="login" xmlns="http://www.w3.org/1999/xhtml">
<head>

	
	<title>Login - OSE</title>

    
    <link href="css/ose.css" rel="stylesheet" /> 

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<script>
		function focar() {
			document.getElementById("usuario").focus();
		}
	</script>
	
</head>
<body onload="focar()">
<form action="j_spring_security_check" method="post" >

	<div id="login-box">
		<div id="login-box-interno">
	    	<div id="login-box-label">Acesso ao Sistema</div>
				<%
				if (request.getParameter("msg") != null) {
					out.print("<span style='color: red;font-weight: bold;'>Usu�rio ou senha incorretos</span>");
				}%>	
			<table id="text-comun">	
			<tr>	
	      		<td><label id="text-comun">Email</label></td>
	      	</tr>
	      	<tr>
	      		<td class="input-div" id="input-usuario">
	      			<input type="text" id="usuario" name="j_username" class="span3"  />
	      		</td>
	      	</tr>
	      	<tr>
	      		<td><label id="text-comun">Senha</label></td>
	      	</tr>
	      	<tr>
	      		<td class="input-div" id="input-senha">
	      			<input name="j_password" type="password" class="span3" />
	      		</td> 
	      	</tr>
	      	<tr>
	      		<td id="botao">
	      			<input class="btn btn-large btn-success btnSignup ose-botao" type="submit" value="Entrar" />	      		
	      		</td>	      	
	      	</tr>
	      	
	      	</table>
	                       
	    </div>
	</div>
            
</form>
</body>
</html>