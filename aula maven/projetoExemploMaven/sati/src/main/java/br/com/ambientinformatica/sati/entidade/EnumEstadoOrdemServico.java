package br.com.ambientinformatica.sati.entidade;

public enum EnumEstadoOrdemServico {
   
   NOVA,
   ATENDENDO,
   ATENDIDA;

}
