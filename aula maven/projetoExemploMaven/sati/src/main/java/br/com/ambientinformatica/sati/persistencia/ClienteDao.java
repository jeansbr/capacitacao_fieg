package br.com.ambientinformatica.sati.persistencia;

import br.com.ambientinformatica.jpa.persistencia.Persistencia;
import br.com.ambientinformatica.sati.entidade.Cliente;

public interface ClienteDao extends Persistencia<Cliente>{

}
