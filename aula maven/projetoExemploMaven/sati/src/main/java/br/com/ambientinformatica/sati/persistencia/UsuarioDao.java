package br.com.ambientinformatica.sati.persistencia;

import br.com.ambientinformatica.sati.entidade.Usuario;
import br.com.ambientinformatica.jpa.persistencia.Persistencia;

public interface UsuarioDao extends Persistencia<Usuario>{

}
