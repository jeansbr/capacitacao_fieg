package br.com.senai.selenium_exemplo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HelloSeleniumTest { 

       @Before
       public void setUp() throws Exception {

       } 

       @Test
       public void testSearchInGooglePage() {

             WebDriver driver  = new FirefoxDriver();            

//           Digo qual url para acessar

             driver.get("http://www.google.com");            

//           Agora vamos buscar o elemento na página

             WebElement inputTextGoogle = driver.findElement(By.name("q"));

             inputTextGoogle.sendKeys("Edjalma Queiroz");        

/*           faz um submit na página

 *           poderia buscar o botão search e fazer o submit tb.

 */
             inputTextGoogle.submit();            
             assertTrue(driver.getPageSource().contains(driver.findElement(By.id("gbqfq")).getText()));
       } 
}